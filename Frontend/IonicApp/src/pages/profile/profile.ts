import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { FooteerTabPage } from '../footeer-tab/footeer-tab';
import { DomSanitizer } from '@angular/platform-browser';
import { TransitionInstruction } from 'ionic-angular/umd/navigation/nav-util';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})

export class ProfilePage {
  senha: string;
  loading: Loading;
  novaImagem: any;
  fotoPerfilAtual: string;
  nomeUsuario: string;
  email: string;
  foto: any;
  options = {
    maximumImagesCount: 1,
    quality: 100
  };

  constructor(public navCtrl: NavController,
              public toastr: ToastController,
              public perfilProvider: AgendamentoProvider,
              public alert: AlertController,
              public loadingCtrl: LoadingController,
              public photoLibrary: PhotoLibrary,
              private imagePicker: ImagePicker,
              private _DomSanitizationService: DomSanitizer,
              private base64: Base64) {

              this.permitirGaleria()
  }

  editaPerfil(){
    this.showLoading()
    this.perfilProvider.alteraSenha(this.senha)
      .subscribe((res) => {
        let toast = this.toastr.create({
          message: 'Senha alterada com sucesso!',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        this.loading.dismiss();
      },
      (err) => {
        this.loading.dismiss()
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present();
      }
    )
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Posicionando satélites...',
    });
    this.loading.present();
  }

  atualizaFotoPerfil(){
    this.showLoading()
    this.perfilProvider.alteraFoto(this.novaImagem)
      .subscribe((res) => {
        localStorage.setItem("foto", this.novaImagem);
        let toast = this.toastr.create({
          message: 'Foto alterada com sucesso!',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        this.loading.dismiss();
      },
      (err) => {
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present();
        this.loading.dismiss();
      }
    )
  }

  abreGaleria(){
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
      }
      this.novaImagem = results[0]
      this.base64.encodeFile(this.novaImagem).then((base64File: string) => {
        this.novaImagem = base64File;
        this.atualizaFotoPerfil();
      }, (err) => {
        console.log(err);
      });
    }, (err) => { });
  }

  permitirGaleria(){
    this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.getLibrary().subscribe({
        next: library => {
          library.forEach(function(libraryItem) {
            console.log(libraryItem.id);          // ID of the photo
            console.log(libraryItem.photoURL);    // Cross-platform access to photo
            console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
            console.log(libraryItem.fileName);
            console.log(libraryItem.width);
            console.log(libraryItem.height);
            console.log(libraryItem.creationDate);
            console.log(libraryItem.latitude);
            console.log(libraryItem.longitude);
            console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
          });
        },
        error: err => { console.log('could not get photos'); },
        complete: () => { console.log('done getting photos'); }
      });
    })
      .catch(err => console.log('permissions weren\'t granted'));
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    this.email = localStorage.getItem("email");
    this.nomeUsuario = localStorage.getItem("nomeUsuario");
    this.foto = localStorage.getItem("foto");
    this.novaImagem = localStorage.getItem("foto")
    this.nomeUsuario = localStorage.getItem("nomeUsuario")
    console.log(this.foto);
  }

  voltaPage(){
    this.navCtrl.setRoot(FooteerTabPage);
  }
}

