import { AgendamentoPage } from './../agendamento/agendamento';
import { Component } from '@angular/core';
import { NavController, MenuController, Loading, LoadingController, AlertController, PopoverController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { AgendamentoProvider } from '../../providers/agendamento/agendamento';
import { PopoverNoticiaComponent } from '../../components/popover-noticia/popover-noticia';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
@ViewChild(Slides) slides: Slides;
  loading: Loading;
  imagem: String;
  subtitulo: String;
​​  titulo: String;
  noticias: any;

  constructor(public navCtrl: NavController,
              public menuCtrl: MenuController,
              public loadingCtrl: LoadingController,
              public agendamento: AgendamentoProvider,
              public alert: AlertController,
              public popoverCtrl: PopoverController
    ) {
  }

  openSideMenu(){
    this.menuCtrl.enable(true)
    this.menuCtrl.toggle();
  }

  rotaAgendamento(){
    this.navCtrl.setRoot(AgendamentoPage);
  }

  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Uffaa... Você aterrizou com sucesso!',
    });
    this.loading.present();
  }


  listaNoticias(){
      this.showLoading()
      this.agendamento.listaHome()
        .subscribe((res) => {
          this.loading.dismiss();
          console.log(res)
          this.noticias = res;
        },
        (err) => {
          let alert = this.alert.create({
            subTitle: err.error.message,
            buttons: ['Voltar']
          });
          alert.present()
          this.loading.dismiss();
        }
      )
  }

  presentPopOver(evento){
    let popover = this.popoverCtrl.create(PopoverNoticiaComponent, evento);
    popover.present();
  }
}
