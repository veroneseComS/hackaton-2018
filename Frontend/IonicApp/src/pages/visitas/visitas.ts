import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-visitas',
  templateUrl: 'visitas.html',
})
export class VisitasPage {

  loading: Loading;
  agendamentos: any = "observatorio"
  visitasAgendadas: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public agendamentoProvider: AgendamentoProvider,
              public loadingCtrl: LoadingController
              ) {
  }

  ionViewDidLoad() {
    this.listaVisitas()
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Procurando ovnis',
    });
    this.loading.present();
  }

  verificaDiaEvento(data: string){
    var dataFinal;
    var minhaDataAtual = new Date()
    var dia  = minhaDataAtual.getDate().toString(),
    diaF = (dia.length == 1) ? '0'+dia : dia,
    mes  = (minhaDataAtual.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
    mesF = (mes.length == 1) ? '0'+mes : mes,
    anoF = minhaDataAtual.getFullYear();
    dataFinal = diaF+"-"+mesF+"-"+anoF;

    if(data == dataFinal){
      return true;
    }else{
      return false;
    }
  }

  listaVisitas(){
    this.showLoading()
    this.agendamentoProvider.listaVisitas()
      .subscribe((res) => {
        this.visitasAgendadas = res;
        for(let i=0; i<this.visitasAgendadas.length;i++){
          var mes = 0;
          var dia = 0;
          var ano = 0;
          mes = this.visitasAgendadas[i].data.substring(5,7)
          dia = this.visitasAgendadas[i].data.substring(8,10);
          ano = this.visitasAgendadas[i].data.substring(0,4);
          this.visitasAgendadas[i].data = dia+"-"+mes+"-"+ano;
          var dataJS = new Date(ano,mes - 1,dia);
          var diaSemana = dataJS.getDay();
          if(diaSemana == 0){
            this.visitasAgendadas[i].tipo = "DOM"
          }
          else if(diaSemana == 1){
            this.visitasAgendadas[i].tipo = "SEG"
          }
          else if(diaSemana == 2){
            this.visitasAgendadas[i].tipo = "TER"
          }
          else if(diaSemana == 3){
            this.visitasAgendadas[i].tipo = "QUA"
          }
          else if(diaSemana == 4){
            this.visitasAgendadas[i].tipo = "QUI"
          }
          else if(diaSemana == 5){
            this.visitasAgendadas[i].tipo = "SEX"
          }
          else if(diaSemana == 6){
            this.visitasAgendadas[i].tipo = "SAB"
          }
        }
        this.loading.dismiss();
      },
      (err) => {
        console.log(err)
        this.loading.dismiss();
      }
    )
  }
}
