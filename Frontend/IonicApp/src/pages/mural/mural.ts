import { Component } from '@angular/core';
import { NavController, Loading, AlertController, LoadingController } from 'ionic-angular';
import { PostPage } from '../post/post';
import { AgendamentoProvider } from '../../providers/agendamento/agendamento';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-mural',
  templateUrl: 'mural.html'
})
export class MuralPage {

    habilitaBotao:boolean;
    cards: any;
    category: string = 'visitations';
    loading: Loading;
    noticias: any;
    novaImagem: any;

    constructor(private socialSharing: SocialSharing, public loadingCtrl: LoadingController,public alert: AlertController, public navCtrl: NavController, public noticiasProvidas: AgendamentoProvider) {
        let tipo_user = localStorage.getItem("tipo_user");
        if(tipo_user == "1"){
          this.habilitaBotao = false;
        }else{
          this.habilitaBotao = true;
        }
        this.listaFotosMural();
    }

    divulgaFoto(){
      this.socialSharing.shareWithOptions({message: "Hey, postamos uma nova foto no nosso app! Baixe agora para ficar por dentro das noticias www.google.com.br"});
    }

    curtePublicacao(noticia, indice){
        this.noticiasProvidas.likeNoticia(noticia)
          .subscribe((res) => {
            if(res.message == "Foto descurtida"){
              this.noticias[indice].euCurti = false;
              this.noticias[indice].curtidas = this.noticias[indice].curtidas - 1;
            }else{
              this.noticias[indice].euCurti = true;
              this.noticias[indice].curtidas = this.noticias[indice].curtidas + 1;
            }
          },
          (err) => {
            let alert = this.alert.create({
              subTitle: err.error.message,
              buttons: ['Voltar']
            });
            alert.present();
            this.loading.dismiss();
          }
        )
      }

  listaFotosMural(){
    this.showLoading()
    this.noticiasProvidas.listaNoticias()
      .subscribe((res) => {
        this.noticias = res;
        console.log(this.noticias);
        this.loading.dismiss();
      },
      (err) => {
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present();
        this.loading.dismiss();
      }
    )
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Posicionando satélites...'
    });
    this.loading.present();
  }

  publicarFoto() {
    this.navCtrl.setRoot(PostPage);
  }
}
