import { MinhaAgendaPage } from './../minha-agenda/minha-agenda';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FooteerTabPage } from './footeer-tab';
import { ProfilePage } from '../profile/profile';
import { SobrePage } from '../sobre/sobre';


@NgModule({
  declarations: [
    FooteerTabPage,
  ],
  imports: [
    IonicPageModule.forChild(FooteerTabPage),
    ProfilePage,
    SobrePage,
    MinhaAgendaPage
  ],
})
export class FooteerTabPageModule {

}
