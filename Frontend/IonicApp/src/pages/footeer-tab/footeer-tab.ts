import { VisitasPage } from './../visitas/visitas';
import { AgendamentoPage } from './../agendamento/agendamento';
import { MuralPage } from './../mural/mural';
import { AboutPage } from './../about/about';
import { LoginPage } from './../login/login';
import { HomePage } from './../home/home';
import { NoticiasPage } from './../noticias/noticias';
import { EventosPage } from './../eventos/eventos';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-footeer-tab',
  templateUrl: 'footeer-tab.html',
})
export class FooteerTabPage {

  visitasPage = VisitasPage
  homePage = HomePage;
  photosPage = MuralPage;
  agendamentoPage = AgendamentoPage;
  wikiPage = AboutPage
  noticiasPage = NoticiasPage;
  eventosPage = EventosPage;
  usuarioComum: boolean = false;
  usuarioEspecial: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(): void {
    let tipoUser = localStorage.getItem("tipo_user");
    if(tipoUser == "1"){
      this.usuarioComum = true;
    }else{
      this.usuarioEspecial = true;
    }
  }

}
