import { Base64 } from '@ionic-native/base64';
import { ImagePicker } from '@ionic-native/image-picker';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, AlertController, Loading, LoadingController, ToastController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { HomePage } from '../home/home';
import { AgendamentoProvider } from '../../providers/agendamento/agendamento';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-noticias',
  templateUrl: 'noticias.html'
})
export class NoticiasPage {
  foto: any;
  loading: Loading;
  tituloNoticia: string;
  subTituloNoticia: string;
  textoNoticia: string;
  novaImagem: string;

  options = {
    maximumImagesCount: 1,
    quality: 100
  };
    constructor(public modalCtrl: ModalController,
                public navCtrl: NavController,
                private alertCtrl: AlertController,
                public navParams: NavParams,
                private dialogs: Dialogs,
                public photoLibrary: PhotoLibrary,
                private imagePicker: ImagePicker,
                private base64: Base64,
                public toastCtrl: ToastController,
                public alert: AlertController,
                public loadingCtrl: LoadingController,
                private _DomSanitizationService: DomSanitizer,
                public noticiaProvider: AgendamentoProvider){

                this.permitirGaleria();
    }

    abreGaleria(){
      this.imagePicker.getPictures(this.options).then((results) => {
        for (var i = 0; i < results.length; i++) {
            console.log('Image URI: ' + results[i]);
        }
        this.novaImagem = results[0]
        this.base64.encodeFile(this.novaImagem).then((base64File: string) => {
          this.novaImagem = base64File;
        }, (err) => {
          console.log(err);
        });
      }, (err) => { });
    }

    permitirGaleria(){
      this.photoLibrary.requestAuthorization().then(() => {
        this.photoLibrary.getLibrary().subscribe({
          next: library => {
            library.forEach(function(libraryItem) {
              console.log(libraryItem.id);          // ID of the photo
              console.log(libraryItem.photoURL);    // Cross-platform access to photo
              console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
              console.log(libraryItem.fileName);
              console.log(libraryItem.width);
              console.log(libraryItem.height);
              console.log(libraryItem.creationDate);
              console.log(libraryItem.latitude);
              console.log(libraryItem.longitude);
              console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
            });
          },
          error: err => { console.log('could not get photos'); },
          complete: () => { console.log('done getting photos'); }
        });
      })
      .catch(err => console.log('permissions weren\'t granted'));
    }

    rotaHome(){
      this.navCtrl.setRoot(HomePage);
    }

    showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Pesquisando novas galáxias...',
      });
      this.loading.present();
    }

    cadastraNoticia(){
      this.showLoading()
      this.noticiaProvider.cadastraNoticia(this.tituloNoticia, this.subTituloNoticia, this.textoNoticia, this.novaImagem)
        .subscribe((res) => {
          this.loading.dismiss();
          let toast = this.toastCtrl.create({
            message: 'Notícia adicionada com sucesso!',
            duration: 2000,
            position: 'bottom'
          });
          toast.present();
          this.tituloNoticia = null;
          this.subTituloNoticia = null;
          this.textoNoticia = null;
        },
        (err) => {
          let alert = this.alert.create({
            subTitle: err.error.message,
            buttons: ['Voltar']
          });
          alert.present();
          this.loading.dismiss();
        }
      )
    }

    presentConfirm() {
      let alert = this.alertCtrl.create({
        subTitle: 'Notícia publicada com sucesso!',
        buttons: [ 'Okay, Obrigado' ]
      });
      alert.present();
    }
}
