import { PhotoLibrary } from '@ionic-native/photo-library';
import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiasPage } from './noticias';


@NgModule({
  declarations: [
    NoticiasPage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiasPage),
  ],
  providers:[
    AgendamentoProvider,
    PhotoLibrary
  ]
})
export class NoticiasPageModule {}
