import { VisitasPage } from './../visitas/visitas';
import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, AlertController, Loading, LoadingController, ToastController, Alert } from 'ionic-angular';
import { HomePage } from '../home/home';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html'
})
export class EventosPage {

  dataEvento: string;
  horaInicio: string;
  horaFinal: string;
  local: string;
  tituloEvento: string;
  descricaoEvento: string;
  sobreEvento: string;
  valor: number;
  numeroVagas: number;
  loading: Loading
  foto: string;
  options = {
    maximumImagesCount: 1,
    quality: 100
  };

    constructor(public navCtrl: NavController,
                private alertCtrl: AlertController,
                public agendamentoProvider: AgendamentoProvider,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController,
                public alert: AlertController,
                public photoLibrary: PhotoLibrary,
                private imagePicker: ImagePicker,
                private base64: Base64) {

                this.permitirGaleria();
    }

    permitirGaleria(){
      this.photoLibrary.requestAuthorization().then(() => {
        this.photoLibrary.getLibrary().subscribe({
          next: library => {
            library.forEach(function(libraryItem) {
              console.log(libraryItem.id);          // ID of the photo
              console.log(libraryItem.photoURL);    // Cross-platform access to photo
              console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
              console.log(libraryItem.fileName);
              console.log(libraryItem.width);
              console.log(libraryItem.height);
              console.log(libraryItem.creationDate);
              console.log(libraryItem.latitude);
              console.log(libraryItem.longitude);
              console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
            });
          },
          error: err => { console.log('could not get photos'); },
          complete: () => { console.log('done getting photos'); }
        });
      })
      .catch(err => console.log('permissions weren\'t granted'));
    }

    abreGaleria(){
      this.imagePicker.getPictures(this.options).then((results) => {
        for (var i = 0; i < results.length; i++) {
            console.log('Image URI: ' + results[i]);
        }
        this.foto = results[0]
        this.base64.encodeFile(this.foto).then((base64File: string) => {

        this.foto = "data:image/jpeg;base64,"+base64File
        }, (err) => {
          console.log(err);
        });
      }, (err) => { });
    }

    rotaHome(){
      this.navCtrl.setRoot(HomePage);
    }

    showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Contando estrelas...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }

    cadastraEvento(){
      this.showLoading()
      this.agendamentoProvider.cadastraEvento(this.numeroVagas, this.valor, this.sobreEvento, this.descricaoEvento, this.tituloEvento, this.local, this.dataEvento, this.horaInicio, this.horaFinal)
        .subscribe((res) => {
          console.log(res)
          this.loading.dismiss();
            let toast = this.toastCtrl.create({
              message: 'Evento adicionado com sucesso!',
              duration: 2000,
              position: 'bottom'
            });
            toast.present();
            this.navCtrl.setRoot(VisitasPage)
          },
        (err) => {
          let alert = this.alert.create({
            subTitle: err.error.message,
            buttons: ['Voltar']
          });
          alert.present()
          this.loading.dismiss();
        }
      )
    }
}
