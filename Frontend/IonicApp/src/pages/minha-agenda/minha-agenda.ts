import { PopoverPageAgendaComponent } from './../../components/popover-page-agenda/popover-page-agenda';
import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, ViewController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { FooteerTabPage } from '../footeer-tab/footeer-tab';


@IonicPage()
@Component({
  selector: 'page-minha-agenda',
  templateUrl: 'minha-agenda.html',
})
export class MinhaAgendaPage {

  loading: Loading;
  visitasAgendadas: any;
  dia: number;
  mes: number;
  ano: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public agendamentoProvider: AgendamentoProvider,
              public alert: AlertController,
              public popoverCtrl: PopoverController
              ) {
  }

  ionViewDidLoad() {
    this.consultaAgenda()
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Distorcendo espaço-tempo...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  verificaDiaEvento(data: string){
    var dataFinal;
    var minhaDataAtual = new Date()
    var dia  = minhaDataAtual.getDate().toString(),
    diaF = (dia.length == 1) ? '0'+dia : dia,
    mes  = (minhaDataAtual.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
    mesF = (mes.length == 1) ? '0'+mes : mes,
    anoF = minhaDataAtual.getFullYear();
    dataFinal = diaF+"-"+mesF+"-"+anoF;

    if(data == dataFinal){
      return true;
    }else{
      return false;
    }
  }

  presentPopover(agendamento) {
    let popover = this.popoverCtrl.create(PopoverPageAgendaComponent, agendamento);
    popover.present();
  }

  consultaAgenda(){
      this.showLoading()
      this.agendamentoProvider.consultaAgenda()
        .subscribe((res) => {
          this.visitasAgendadas = res;
          for(let i=0; i<this.visitasAgendadas.length;i++){

            var mes = 0;
            var dia = 0;
            var ano = 0;
            mes = this.visitasAgendadas[i].data.substring(5,7)
            dia = this.visitasAgendadas[i].data.substring(8,10);
            ano = this.visitasAgendadas[i].data.substring(0,4);
            this.visitasAgendadas[i].data = dia+"/"+mes+"-"+ano;
            var dataJS = new Date(ano,mes - 1,dia);
            var diaSemana = dataJS.getDay();
            if(diaSemana == 0){
              this.visitasAgendadas[i].tipo = "DOM"
            }
            else if(diaSemana == 1){
              this.visitasAgendadas[i].tipo = "SEG"
            }
            else if(diaSemana == 2){
              this.visitasAgendadas[i].tipo = "TER"
            }
            else if(diaSemana == 3){
              this.visitasAgendadas[i].tipo = "QUA"
            }
            else if(diaSemana == 4){
              this.visitasAgendadas[i].tipo = "QUI"
            }
            else if(diaSemana == 5){
              this.visitasAgendadas[i].tipo = "SEX"
            }
            else if(diaSemana == 6){
              this.visitasAgendadas[i].tipo = "SAB"
            }
          }
        },
        (err) => {
          let alert = this.alert.create({
            subTitle: err.error.message,
            buttons: ['Voltar']
          });
          alert.present();
          this.loading.dismiss();
        }
      )
    }

    voltaPage(){
      this.navCtrl.setRoot(FooteerTabPage);
    }
}
