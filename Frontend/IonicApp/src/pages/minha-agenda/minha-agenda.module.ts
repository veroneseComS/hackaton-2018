import { PopoverPageAgendaComponent } from './../../components/popover-page-agenda/popover-page-agenda';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhaAgendaPage } from './minha-agenda';


@NgModule({
  declarations: [
    MinhaAgendaPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhaAgendaPage),
  ],
  entryComponents:[
    MinhaAgendaPage,
    PopoverPageAgendaComponent
  ],
  exports:[
    MinhaAgendaPage
  ]
})
export class MinhaAgendaPageModule {}
