import { PopoverEventoAgendamentoComponent } from './../../components/popover-evento-agendamento/popover-evento-agendamento';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendamentoPage } from './agendamento';


@NgModule({
  declarations: [
    AgendamentoPage,
    PopoverEventoAgendamentoComponent
  ],
  imports: [
    IonicPageModule.forChild(AgendamentoPage),
  ],
  entryComponents:[
    PopoverEventoAgendamentoComponent
  ]
})
export class AgendamentoPageModule {}
