import { PopoverEventoAgendamentoComponent } from './../../components/popover-evento-agendamento/popover-evento-agendamento';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, AlertController, Loading, LoadingController, PopoverController } from 'ionic-angular';
import { Dialogs } from '@ionic-native/dialogs';
import { ModalAgendamentoPage } from '../modal-agendamento/modal-agendamento';
import { AgendamentoProvider } from '../../providers/agendamento/agendamento';

@IonicPage()
@Component({
  selector: 'page-agendamento',
  templateUrl: 'agendamento.html',
})
export class AgendamentoPage {
  currentEvents = [];
  visitasAgendadas: any;
  anoSelecionado: number;
  mesSelecionado: number;
  diaSelecionado: number;
  loading: Loading;
  constructor(public popoverCtrl: PopoverController, public alert: AlertController, public agendamento: AgendamentoProvider, public loadingctrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, private dialogs: Dialogs, public http: HttpClient) {
    this.listaEventosAdmin();
  }

  voltaPage(){
  }

  verificaDiaEvento(data: string){
    var dataFinal;
    var minhaDataAtual = new Date()
    var dia  = minhaDataAtual.getDate().toString(),
    diaF = (dia.length == 1) ? '0'+dia : dia,
    mes  = (minhaDataAtual.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
    mesF = (mes.length == 1) ? '0'+mes : mes,
    anoF = minhaDataAtual.getFullYear();
    dataFinal = diaF+"-"+mesF+"-"+anoF;

    if(data == dataFinal){
      return true;
    }else{
      return false;
    }
  }

  showLoading(){
    this.loading = this.loadingctrl.create({
      content: 'Descobrindo novos planetas...'
    });
    this.loading.present();
  }

  presentPopover(evento){
    let popover = this.popoverCtrl.create(PopoverEventoAgendamentoComponent, evento);
    popover.present();
  }

  listaEventosAdmin(){
    this.agendamento.listaEventosAdmin()
      .subscribe((res) => {
        this.currentEvents = res;
        this.visitasAgendadas = JSON.parse(JSON.stringify(res));
        for(let i=0; i<this.visitasAgendadas.length;i++){
          var mes = 0;
          var dia = 0;
          var ano = 0;
          mes = this.visitasAgendadas[i].month;
          dia = this.visitasAgendadas[i].date;
          ano = this.visitasAgendadas[i].year;
          this.visitasAgendadas[i].date = dia+"/"+mes+"/"+ano;
          var dataJS = new Date(ano,mes,dia);
          var diaSemana = dataJS.getDay();
          if(diaSemana == 0){
            this.visitasAgendadas[i].tipo = "DOM"
          }
          else if(diaSemana == 1){
            this.visitasAgendadas[i].tipo = "SEG"
          }
          else if(diaSemana == 2){
            this.visitasAgendadas[i].tipo = "TER"
          }
          else if(diaSemana == 3){
            this.visitasAgendadas[i].tipo = "QUA"
          }
          else if(diaSemana == 4){
            this.visitasAgendadas[i].tipo = "QUI"
          }
          else if(diaSemana == 5){
            this.visitasAgendadas[i].tipo = "SEX"
          }
          else if(diaSemana == 6){
            this.visitasAgendadas[i].tipo = "SAB"
          }
        }
      },
      (err) => {
        console.log(err);
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present()
      }
    )
  }

  ionViewDidLoad() {
  }


  onDaySelect(event) {
    console.log(event);
    this.anoSelecionado = event.year;
    this.mesSelecionado = event.month + 1;
    this.diaSelecionado = event.date;
    let alert = this.alertCtrl.create({
      title: null,
      cssClass: 'buttonCss',
      message: 'Deseja agendar uma visita?',
      buttons: [
        {
          text: 'Não, obrigado',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          role: 'sim',
          handler: () =>{ this.presentProfileModal()
        }
      }
      ]
    });
    alert.present();
  }

  presentProfileModal() {
    let profileModal = this.modalCtrl.create(ModalAgendamentoPage,{ano: this.anoSelecionado, mes: this.mesSelecionado, dia: this.diaSelecionado},{enableBackdropDismiss: true});
    profileModal.present();
  }
}
