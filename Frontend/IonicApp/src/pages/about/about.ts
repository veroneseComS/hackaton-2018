import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(){}

  textoBtn1: string = "Leia mais...";
  textoBtn2: string = "Leia mais...";
  textoBtn3: string = "Leia mais...";

  settings: any = {
    lang: 'pt-BR',
    theme: 'material'
  }

  trocaTexto(btnReferencia: number){
    if(btnReferencia == 1){
      if(this.textoBtn1 == "Leia mais..."){
        this.textoBtn1 = "Ocultar"
      }else{
        this.textoBtn1 = "Leia mais..."
      }
    }else if(btnReferencia == 2){
      if(this.textoBtn2 == "Leia mais..."){
        this.textoBtn2 = "Ocultar"
      }else{
        this.textoBtn2 = "Leia mais..."
      }
    }
    else if(btnReferencia == 3){
      if(this.textoBtn3 == "Leia mais..."){
        this.textoBtn3 = "Ocultar"
      }else{
        this.textoBtn3 = "Leia mais..."
      }
    }
  }
}
