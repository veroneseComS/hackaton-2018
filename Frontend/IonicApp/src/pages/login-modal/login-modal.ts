import { FooteerTabPage } from './../footeer-tab/footeer-tab';
import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, ModalController, ViewController, Events } from 'ionic-angular';
import { Usuario } from '../../models/usuario.model';
import { LoginPage } from '../login/login';
import { HttpClient } from '@angular/common/http';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-login-modal',
  templateUrl: 'login-modal.html',
})
export class LoginModalPage {

  usuario: Usuario = new Usuario();
  loading: Loading;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: HttpClient,
              public loadingCtrl: LoadingController,
              public auth: AuthProvider,
              public alert: AlertController,
              public modal: ModalController,
              public viewCtrl: ViewController,
              public events: Events
              ) {}


  voltaLogin(){
    this.navCtrl.popAll();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Posicionando satélites...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  login():void{
      this.showLoading()
      this.auth.login(this.usuario)
        .subscribe((res) => {
          console.log(res);
          this.loading.dismiss();
          if(res.token != null){
            localStorage.setItem("token", res.token);
            localStorage.setItem("email", res.usuario.email);
            localStorage.setItem("nomeUsuario", this.usuario.nomeUsuario);
            localStorage.setItem("tipo_user", res.usuario.tipo);
            localStorage.setItem("foto", res.usuario.foto);
            this.viewCtrl.dismiss().then(() => {
              this.events.publish('user:changed');
            })
          }
        },
        (err) => {
          let alert = this.alert.create({
            subTitle: err.error.message,
            buttons: ['Voltar']
          });
          alert.present();
          this.loading.dismiss();
        }
      )
    }
  }
