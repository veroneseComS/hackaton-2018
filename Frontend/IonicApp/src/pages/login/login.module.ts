import { LoginModalPage } from './../login-modal/login-modal';
import { RegistroModalPage } from './../registro-modal/registro-modal';
import { ModalAgendamentoPage } from './../modal-agendamento/modal-agendamento';
import { AuthProvider } from './../../providers/auth/auth';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
  providers:[
    AuthProvider
  ],
  entryComponents:[RegistroModalPage,LoginModalPage]
})
export class LoginPageModule {}
