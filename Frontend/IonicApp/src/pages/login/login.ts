import { LoginModalPage } from './../login-modal/login-modal';
import { AuthProvider } from './../../providers/auth/auth';
import { FooteerTabPage } from './../footeer-tab/footeer-tab';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegistroModalPage } from '../registro-modal/registro-modal';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public authService: AuthProvider,
              public modalCtrl: ModalController,
              public events: Events
              ) {
  }

  modalLogin(){
    let modalLogin = this.modalCtrl.create(LoginModalPage);
    modalLogin.present();
  }

  modalRegistro(){
      let modalRegistro = this.modalCtrl.create(RegistroModalPage,{enableBackdropDismiss: true, showBackdrop: true});
      modalRegistro.present();
    }
  }
