import { MuralPage } from './../mural/mural';
import { Component } from '@angular/core';
import { NavController, AlertController, Loading, LoadingController, ToastController } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { DomSanitizer } from '@angular/platform-browser';
import { AgendamentoProvider } from '../../providers/agendamento/agendamento';

@Component({
  selector: 'page-post',
  templateUrl: 'post.html'
})

export class PostPage {

  descricao: string;
  settings: any = {
    lang: 'pt-BR',
    theme: 'material'
  }
  loading: Loading;
  novaImagem: any;
  options = {
    maximumImagesCount: 1,
    quality: 100
  };

  constructor(public navCtrl: NavController,
              public alert: AlertController,
              public loadingctrl: LoadingController,
              public photoLibrary: PhotoLibrary,
              private imagePicker: ImagePicker,
              private base64: Base64,
              private _DomSanitizationService: DomSanitizer,
              private postProvider: AgendamentoProvider,
              public toastr: ToastController
  ) {

    this.permitirGaleria();
  }

  abreGaleria(){
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
      }
      this.novaImagem = results[0]
      this.base64.encodeFile(this.novaImagem).then((base64File: string) => {
        this.novaImagem = base64File;
      }, (err) => {
        console.log(err);
      });
    }, (err) => { });
  }

  permitirGaleria(){
    this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.getLibrary().subscribe({
        next: library => {
          library.forEach(function(libraryItem) {
            console.log(libraryItem.id);          // ID of the photo
            console.log(libraryItem.photoURL);    // Cross-platform access to photo
            console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
            console.log(libraryItem.fileName);
            console.log(libraryItem.width);
            console.log(libraryItem.height);
            console.log(libraryItem.creationDate);
            console.log(libraryItem.latitude);
            console.log(libraryItem.longitude);
            console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
          });
        },
        error: err => { console.log('could not get photos'); },
        complete: () => { console.log('done getting photos'); }
      });
    })
      .catch(err => console.log('permissions weren\'t granted'));
  }

  showLoading() {
    this.loading = this.loadingctrl.create({
      content: 'Checando nave-mãe...',
      duration: 2000
    });
    this.loading.present();
  }

  publicarFoto(){
    this.showLoading()
    this.postProvider.cadastraMural(this.novaImagem, this.descricao)
      .subscribe((res) => {
        let toast = this.toastr.create({
          message: 'Publicado com sucesso',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        this.novaImagem = null;
        this.descricao = null;
        this.loading.dismiss();
      },
      (err)=>{
        this.loading.dismiss();
        console.log(err);
      })}

  cancelarFoto(){
    this.navCtrl.setRoot(MuralPage);
  }

}
