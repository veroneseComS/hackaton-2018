import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { PostPage } from './post';


@NgModule({
  declarations: [
    PostPage,
  ],
  imports: [
    IonicPageModule.forChild(PostPage),
  ],
  providers: [
    PhotoLibrary,
  ]
})
export class PostModalPageModule {}
