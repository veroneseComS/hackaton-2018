import { FooteerTabPage } from './../footeer-tab/footeer-tab';
import { HomePage } from './../home/home';
import { AuthProvider } from './../../providers/auth/auth';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './../../models/usuario.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, ViewController, Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-registro-modal',
  templateUrl: 'registro-modal.html',
})
export class RegistroModalPage {

  loading: Loading;
  usuario: Usuario = new Usuario();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: HttpClient,
              public loadingCtrl: LoadingController,
              public auth: AuthProvider,
              private toastCtrl: ToastController,
              public alert: AlertController,
              public viewCtrl: ViewController,
              public events:Events
              ) {
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Distorcendo espaço-tempo...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  volta(){
    this.navCtrl.popAll();
  }

  public registraUsuario(): void {
    this.showLoading()
    this.auth.registraUsuario(this.usuario)
      .subscribe((res) => {
        this.loading.dismiss();
        if(res.token != null){
          localStorage.setItem("token", res.token);
          localStorage.setItem("email", res.usuario.email);
          localStorage.setItem("nomeUsuario", this.usuario.nomeUsuario);
          localStorage.setItem("tipo_user", res.usuario.tipo);
          this.viewCtrl.dismiss().then(() => {
            this.events.publish('user:changed');
          })
          let toast = this.toastCtrl.create({
            message: 'Você foi registrado com sucesso!',
            duration: 2000,
            position: 'bottom'
          });
          toast.present();
        }
      },
      (err) => {
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present()
        this.loading.dismiss();
      }
    )
  }
}
