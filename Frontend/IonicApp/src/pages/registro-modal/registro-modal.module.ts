import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroModalPage } from './registro-modal';

@NgModule({
  declarations: [
    RegistroModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroModalPage),
  ],
})
export class RegistroModalPageModule {}
