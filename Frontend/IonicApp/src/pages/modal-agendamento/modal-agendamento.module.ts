import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAgendamentoPage } from './modal-agendamento';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    ModalAgendamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAgendamentoPage),
    BrMaskerModule
  ],
  providers:[
    AgendamentoProvider
  ]
})
export class ModalAgendamentoPageModule {}
