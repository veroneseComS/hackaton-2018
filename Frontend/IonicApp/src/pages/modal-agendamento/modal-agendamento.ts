import { AgendamentoProvider } from './../../providers/agendamento/agendamento';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@IonicPage()
@Component({
  selector: 'page-modal-agendamento',
  templateUrl: 'modal-agendamento.html',
})
export class ModalAgendamentoPage{

  loading: Loading;
  telefone: string;
  nomePessoa: string;
  nomeInstituicao: string;
  nroPessoas: number;
  atracao: string;
  periodo: number;
  tipoVisita: number;
  ano: number;
  mes: number;
  dia: number;
  nomeDiaSemanaSelecionado:string;
  nomeMesSelecionado:string;

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public loadingctrl: LoadingController,
              public agendamentoProvider: AgendamentoProvider,
              public alert: AlertController,
              public toastCtrl: ToastController
              ) {
    this.ano = params.get('ano');
    this.mes = params.get('mes');
    this.dia = params.get('dia');

    this.nomeDiaSemanaSelecionado = this.getNomeDiaSemana();
    if(this.mes == 1){
      this.nomeMesSelecionado = "Janeiro"
    }
    else if(this.mes == 2){
      this.nomeMesSelecionado = "Fevereiro"
    }
    else if(this.mes == 3){
      this.nomeMesSelecionado = "Março"
    }
    else if(this.mes == 4){
      this.nomeMesSelecionado = "Abril"
    }
    else if(this.mes == 5){
      this.nomeMesSelecionado = "Maio"
    }
    else if(this.mes == 6){
      this.nomeMesSelecionado = "Junho"
    }
    else if(this.mes == 7){
      this.nomeMesSelecionado = "Julho"
    }
    else if(this.mes == 8){
      this.nomeMesSelecionado = "Agosto"
    }
    else if(this.mes == 9){
      this.nomeMesSelecionado = "Setembro"
    }
    else if(this.mes == 10){
      this.nomeMesSelecionado = "Outubro"
    }
    else if(this.mes == 11){
      this.nomeMesSelecionado = "Novembro"
    }
    else if(this.mes == 12){
      this.nomeMesSelecionado = "Dezembro"
    }

  }

  showLoading() {
    this.loading = this.loadingctrl.create({
      content: 'Colocando os cintos...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  voltaPage(){
    this.navCtrl.pop();
  }

  cadastraEvento(){
    this.showLoading()
    this.agendamentoProvider.cadastraVisita(this.telefone, this.nomePessoa, this.nomeInstituicao, this.nroPessoas, this.atracao, this.periodo, this.tipoVisita, this.ano, this.dia, this.mes)
      .subscribe((res) => {
        this.loading.dismiss();
        let toast = this.toastCtrl.create({

          message: 'Visita agendada com sucesso, aguardamos você!',
          duration: 2000,
          position: 'bottom'
        });
        toast.present();
        },
      (err) => {
        this.loading.dismiss();
        let alert = this.alert.create({
          subTitle: err.error.message,
          buttons: ['Voltar']
        });
        alert.present();
      }
    )
  }

  getNomeDiaSemana() {
    //Algoritmo para descobrir o dia da semana que o usuário selecionou
    let a = ((12 - this.mes) / 10);
    let b = (this.ano - a);
    let c = this.mes + (12 * a);
    let d = b / 100;
    let e = d / 4;
    let f = 2 - d + e;
    let g = Math.trunc(365.25 * b);
    let h = Math.trunc(30.6001 * (c + 1));
    let i = Math.trunc((f + g) + (h + this.dia) + 5);
    let j = Math.trunc(i % 7); //Resto de I por 7, onde 0=SAB, 1=DOM, 2=SEG, 3=TER, 4=QUA, 5=QUI, 6=SEX
    //Testa o resultado e retorna
    console.log(j);
    switch (j) {
        case 0:
            return "Sáb";
        case 1:
            return "Dom";
        case 2:
            return "Seg";
        case 3:
            return "Ter";
        case 4:
            return "Qua";
        case 5:
            return "Qui";
        case 6:
            return "Sex";
        default:
            return "Erro ao tentar retornar o dia da semana";
    }
}

}
