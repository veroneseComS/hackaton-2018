import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FooteerTabPage } from '../footeer-tab/footeer-tab';

@Component({
  selector: 'page-sobre',
  templateUrl: 'sobre.html'
})
export class SobrePage {
  settings: any = {
    lang: 'pt-BR',
    theme: 'material'
  }

  constructor(public navCtrl: NavController){

  }

  voltaPage(){
    this.navCtrl.setRoot(FooteerTabPage);
  }
}

