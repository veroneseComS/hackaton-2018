import { MinhaAgendaPage } from './../pages/minha-agenda/minha-agenda';
import { AgendamentoPage } from './../pages/agendamento/agendamento';
import { LoginPage } from './../pages/login/login';
import { FooteerTabPage } from './../pages/footeer-tab/footeer-tab';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ViewController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { ProfilePage } from '../pages/profile/profile';
import { SobrePage } from '../pages/sobre/sobre';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  nomeUsuario: string;
  email: string;
  foto: string;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;
  pages2: any;

  constructor(public events: Events,
              public platform: Platform,
              public statusBar: StatusBar,
              private _DomSanitizationService: DomSanitizer,
              public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for navigation menu
    this.pages = [
      { title: 'Perfil', component: ProfilePage },
      { title: 'Minha agenda', component: HomePage },
      { title: 'Sobre', component: SobrePage },
      { title: 'Sair', component: HomePage }
    ];
    // serve para chamar a rota e adicionar um icon
    this.pages2 = {
      perfil: ProfilePage,
      sobre: SobrePage,
      sair: LoginPage,
    }

    events.subscribe('user:changed', () => {
      this.nav.setRoot(FooteerTabPage);
    });

  }

  ngAfterContentInit(): void {
    this.email = localStorage.getItem("email");
    this.nomeUsuario = localStorage.getItem("nomeUsuario");
    this.foto = localStorage.getItem("foto");
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if(localStorage.getItem("token") != null){
        this.nav.setRoot(FooteerTabPage);
      }else{
        this.nav.setRoot(LoginPage);
      }

    });
  }

  logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    localStorage.removeItem("nomeUsuario");
    localStorage.removeItem("tipo_user");
    localStorage.removeItem("foto");
    this.nav.setRoot(LoginPage);
  }

  rotaAgenda(){
    this.nav.setRoot(MinhaAgendaPage);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
