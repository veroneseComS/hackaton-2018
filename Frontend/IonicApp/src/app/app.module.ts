import { SocialSharing } from '@ionic-native/social-sharing';
import { PopoverEventoAgendamentoComponent } from './../components/popover-evento-agendamento/popover-evento-agendamento';
import { PopoverPageAgendaComponent } from './../components/popover-page-agenda/popover-page-agenda';
import { MinhaAgendaPage } from './../pages/minha-agenda/minha-agenda';
import { AgendamentoProvider } from './../providers/agendamento/agendamento';
import { LoginModalPageModule } from './../pages/login-modal/login-modal.module';
import { LoginModalPage } from './../pages/login-modal/login-modal';
import { RegistroModalPageModule } from './../pages/registro-modal/registro-modal.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalAgendamentoPage } from './../pages/modal-agendamento/modal-agendamento';
import { Dialogs } from '@ionic-native/dialogs';
import { LoginPageModule } from './../pages/login/login.module';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, ViewController } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MuralPage } from '../pages/mural/mural';
import { AboutPage } from '../pages/about/about';
import { AgendamentoPage } from './../pages/agendamento/agendamento';
import { FooteerTabPage } from './../pages/footeer-tab/footeer-tab';
import { VisitasPage } from './../pages/visitas/visitas';
import { NoticiasPage } from './../pages/noticias/noticias';
import { EventosPage } from './../pages/eventos/eventos';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CalendarModule } from 'ionic3-calendar-en';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { AuthProvider } from '../providers/auth/auth';
import { ProfilePage } from '../pages/profile/profile';
import { SobrePage } from '../pages/sobre/sobre';
import { PostPage } from '../pages/post/post';
import { Camera } from '@ionic-native/camera';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

import { registerLocaleData } from '@angular/common';
import localeBr from '@angular/common/locales/br';
import { PostModalPageModule } from '../pages/post/post.module';
import { PopoverNoticiaComponent } from '../components/popover-noticia/popover-noticia';
registerLocaleData(localeBr, 'br');

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FooteerTabPage,
    MuralPage,
    AgendamentoPage,
    ModalAgendamentoPage,
    AboutPage,
    VisitasPage,
    ProfilePage,
    NoticiasPage,
    EventosPage,
    SobrePage,
    MinhaAgendaPage,
    PopoverPageAgendaComponent,
    PopoverEventoAgendamentoComponent,
    PopoverNoticiaComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CalendarModule,
    LoginPageModule,
    BrMaskerModule,
    HttpClientModule,
    RegistroModalPageModule,
    LoginModalPageModule,
    PostModalPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MinhaAgendaPage,
    ListPage,
    VisitasPage,
    LoginPage,
    FooteerTabPage,
    MuralPage,
    PopoverPageAgendaComponent,
    AgendamentoPage,
    ModalAgendamentoPage,
    AboutPage,
    ProfilePage,
    NoticiasPage,
    EventosPage,
    SobrePage,
    LoginModalPage,
    PostPage,
    PopoverEventoAgendamentoComponent,
    PopoverNoticiaComponent
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    StatusBar,
    Dialogs,
    SocialSharing,
    SplashScreen,
    HttpClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    AgendamentoProvider,
    Camera,
    PhotoLibrary,
    ImagePicker,
    Base64
  ]
})
export class AppModule {}
