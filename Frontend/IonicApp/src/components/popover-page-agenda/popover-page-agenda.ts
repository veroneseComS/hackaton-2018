import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'popover-page-agenda',
  templateUrl: 'popover-page-agenda.html'
})
export class PopoverPageAgendaComponent {

  tipoAgendamento: number;
  nomeResponsavel: string;
  data: string;
  periodo: string;

  constructor(public navParams:NavParams){
    if(this.navParams.data.atracao == "Ciência Móvel"){
      this.tipoAgendamento = 1;
    }
    else if(this.navParams.data.atracao == "Museu Interativo de Ciências"){
      this.tipoAgendamento = 2;
    }
    else if(this.navParams.data.atracao == "Observatório"){
      this.tipoAgendamento = 3;
    }
    this.nomeResponsavel = this.navParams.data.nomeResponsavel;
    this.data = this.navParams.data.data
    this.periodo = this.navParams.data.periodo;
  }
}
