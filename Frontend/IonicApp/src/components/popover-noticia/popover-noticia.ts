import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

/**
 * Generated class for the PopoverNoticiaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover-noticia',
  templateUrl: 'popover-noticia.html'
})
export class PopoverNoticiaComponent {
  subtitulo: string
​​  imagem:string
  titulo: string

  constructor(public navParams:NavParams) {
    this.imagem = this.navParams.data.imagem
    this.subtitulo = this.navParams.data.subtitulo
    this.titulo = this.navParams.data.titulo

  }

}
