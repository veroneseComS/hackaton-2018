import { NgModule } from '@angular/core';
import { PopoverPageAgendaComponent } from './popover-page-agenda/popover-page-agenda';
import { PopoverEventoAgendamentoComponent } from './popover-evento-agendamento/popover-evento-agendamento';
import { PopoverNoticiaComponent } from './popover-noticia/popover-noticia';
@NgModule({
	declarations: [PopoverPageAgendaComponent,
    PopoverEventoAgendamentoComponent,
    PopoverNoticiaComponent],
	imports: [],
	exports: [PopoverPageAgendaComponent,
    PopoverEventoAgendamentoComponent,
    PopoverNoticiaComponent]
})
export class ComponentsModule {}
