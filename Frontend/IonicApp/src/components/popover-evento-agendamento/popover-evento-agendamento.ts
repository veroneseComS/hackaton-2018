import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'popover-evento-agendamento',
  templateUrl: 'popover-evento-agendamento.html'
})
export class PopoverEventoAgendamentoComponent {

  tituloEvento: string;
  dataEvento: string;
  horaInicio: string;
  horaFinal: string;
  sobre: string;
  valor: number;

  constructor(public navParams:NavParams) {
    this.tituloEvento = this.navParams.data.titulo;
    this.sobre = this.navParams.data.sobre;
    this.dataEvento = this.navParams.data.date
    this.horaFinal = this.navParams.data.horaFinal;
    this.horaInicio = this.navParams.data.horaInicio;
    console.log(this.navParams.data);
  }

}
