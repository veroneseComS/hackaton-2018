import { Usuario } from './../../models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient) {
  }

  public registraUsuario(usuario: Usuario){
    return this.http.post<any>("http://agenda-ciencia.tk:3100/usuario/registro",{
      email: usuario.email,
      senha: usuario.senha,
      confirmacaoSenha: usuario.confirmacaosenha
    })
  }

  public login(usuario: Usuario){
    return this.http.post<any>("http://agenda-ciencia.tk:3100/usuario/login",{
      email: usuario.email,
      senha: usuario.senha,
    })
  }

}
