import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

@Injectable()
export class AgendamentoProvider {

  constructor(public http: HttpClient) {
  }

  alteraSenha(senha: string){
    let token = localStorage.getItem("token");
    return this.http.put<any>("http://agenda-ciencia.tk:3100/usuario/alterar-senha",
    {senha: senha, confirmacaoSenha: senha},
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  alteraFoto(imagem){
    let token = localStorage.getItem("token");
    return this.http.put<any>("http://agenda-ciencia.tk:3100/usuario/alterar-foto",
    {foto: imagem},
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  cadastraMural(imagem: string, descricao: string){
    let token = localStorage.getItem("token");
    return this.http.post<any>("http://agenda-ciencia.tk:3100/mural",
    {descricao: descricao, novaImagem: imagem},
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
  )
}

  listaNoticias(){
    let token = localStorage.getItem("token");
      return this.http.get<any>("http://agenda-ciencia.tk:3100/mural?pagina=1&linhas=10",
      {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  likeNoticia(noticia){
    let idCurtir = noticia.id
    let devoCurtir: boolean;
    let token = localStorage.getItem("token");
    if(noticia.euCurti == false){
      devoCurtir = true;
    }else{
      devoCurtir = false;
    }
    return this.http.post<any>("http://agenda-ciencia.tk:3100/mural/"+idCurtir+"/curtir",
    {curtir: devoCurtir},
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)})
  }

  cadastraNoticia(titulo: string, subtitulo: string, texto: string, imagem: string){
    let token = localStorage.getItem("token");
      return this.http.post<any>("http://agenda-ciencia.tk:3100/noticia",
      {
        titulo: titulo,
        subtitulo: subtitulo,
        texto: texto,
        novaImagem: imagem
      },
      {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  listaEventosAdmin(){
    let token = localStorage.getItem("token");
      return this.http.get<any>("http://agenda-ciencia.tk:3100/eventos?ano=2018",
      {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  consultaAgenda(){
    let token = localStorage.getItem("token");
    return this.http.get<any>("http://agenda-ciencia.tk:3100/usuario/minha-agenda?pagina=1&linhas=10",
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  listaVisitas(){
    let token = localStorage.getItem("token");
    return this.http.get<any>("http://agenda-ciencia.tk:3100/visitas",
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }

  cadastraEvento(numeroVagas: number, valor: number, sobreEvento:string, descricaoEvento:string, tituloEvento: string,local: string, dataEvento:string,horaInicio: string, horaFinal:string){
    let token = localStorage.getItem("token");
    return this.http.post<any>("http://agenda-ciencia.tk:3100/evento",
    {
      data: dataEvento,
      titulo: tituloEvento,
      descricao: descricaoEvento,
      sobre: sobreEvento,
      valor: valor,
      vagas: numeroVagas,
      horaInicio: horaInicio,
      horaFinal: horaFinal,
      novaImagem: null
    },
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
  )}

  cadastraVisita(telefone: string, nomePessoa: string, nomeInstituicao: string,
                nroPessoas: number, atracao: string, periodo: number, tipoVisita: number,
                ano: number, dia: number, mes: number
                ){

  let token = localStorage.getItem("token");

  return this.http.post<any>("http://agenda-ciencia.tk:3100/visita",{
    periodo: periodo,
    atracao: atracao,
    tipo: tipoVisita,
    nomeResponsavel: nomePessoa,
    nomeInstituicao: nomeInstituicao,
    telefone: telefone,
    qtdPessoas: nroPessoas,
    data: ano+"-"+mes+"-"+dia
    },
    {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }


  listaHome(){
    let token = localStorage.getItem("token");
      return this.http.get<any>("http://agenda-ciencia.tk:3100/noticias?pagina=1&linhas=10",
      {headers:new HttpHeaders().set('Authorization', 'Bearer ' + token)}
    )
  }
}
