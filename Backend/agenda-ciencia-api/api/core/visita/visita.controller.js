const service = require('./visita.service');

module.exports = {
    listar,
    listarAdmin,
    inserir
};

async function listar(req, res) {
    const params = {
        usuarioId: req.user.id,
        pagina: req.query.pagina,
        linhas: req.query.linhas
    };

    const data = await service.listar(params);

    res.finish(data)
}

async function listarAdmin(req, res) {
    const params = {
        pagina: req.query.pagina,
        linhas: req.query.linhas
    };

    const data = await service.listar(params);

    res.finish(data)
}

async function inserir(req, res) {
    const params = {
        usuarioId: req.user.id,
        periodo: req.body.periodo,
        atracao: req.body.atracao,
        tipo: req.body.tipo,
        nomeResponsavel: req.body.nomeResponsavel,
        telefone: req.body.telefone,
        qtdPessoas: req.body.qtdPessoas,
        nomeInstituicao: req.body.nomeInstituicao,
        data: req.body.data
    };

    const data = await service.inserir(params);

    res.finish({
        code: 0,
        message: 'Visita adicionada',
        return: data
    })
}