/**/


SELECT DeleteFunctions('Agenda_Ciencia', 'VisitaListar');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.VisitaListar(
    pUsuarioId INTEGER,
    pPagina    INTEGER,
    pLinhas    INTEGER
)
    RETURNS TABLE(
        "id"              Agenda_Ciencia.Visita.id%TYPE,
        "periodo"         Agenda_Ciencia.Periodo.nome%TYPE,
        "atracao"         Agenda_Ciencia.Atracao.nome%TYPE,
        "tipo"            Agenda_Ciencia.Tipo_Visita.nome%TYPE,
        "tipo2"           Agenda_Ciencia.Tipo_Visita.nome%TYPE,
        "data"            Agenda_Ciencia.Visita.data%TYPE,
        "qtdPessoas"      Agenda_Ciencia.Visita.qtd_pessoas%TYPE,
        "nomeResponsavel" Agenda_Ciencia.Visita.nome_responsavel%TYPE,
        "nomeInstituicao" Agenda_Ciencia.Visita.nome_instituicao%TYPE,
        "telefone"        Agenda_Ciencia.Visita.telefone%TYPE
    ) AS $$

/*
Documentation
Source file.......: visita.sql
Description.......: Listar visitas
Author............: Arthur Castaldi
Date..............: 26/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.VisitaListar(null, null, null);

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT v.id,
           p.nome  periodo,
           a.nome  atracao,
           tv.nome tipo,
           tv.nome tipo2,
           v.data,
           v.qtd_pessoas,
           v.nome_responsavel,
           v.nome_instituicao,
           v.telefone
    FROM Agenda_Ciencia.Visita v
             INNER JOIN Agenda_Ciencia.Periodo p ON p.id = v.periodo
             INNER JOIN Agenda_Ciencia.Atracao a ON a.id = v.atracao
             INNER JOIN Agenda_Ciencia.Tipo_Visita tv ON tv.id = v.tipo
    WHERE (pUsuarioId IS NULL
               OR v.usuario_cadastro = pUsuarioId)
    ORDER BY v.data DESC, v.data_cadastro DESC
    LIMIT iif(pLinhas > 0 AND pPagina > 0, pLinhas, NULL)
    OFFSET iif(pLinhas > 0 AND pPagina > 0, (pPagina - 1) * pLinhas, NULL);
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'VisitaInserir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.VisitaInserir(
    pUsuarioId       Agenda_Ciencia.Usuario.id%TYPE,
    pPeriodo         INTEGER,
    pAtracao         INTEGER,
    pTipo            INTEGER,
    pNomeResponsavel Agenda_Ciencia.Visita.nome_responsavel%TYPE,
    pTelefone        Agenda_Ciencia.Visita.telefone%TYPE,
    pQtdPessoas      INTEGER,
    pNomeInstituicao Agenda_Ciencia.Visita.nome_instituicao%TYPE,
    pData            Agenda_Ciencia.Visita.data%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: visita.sql
Description.......: Inserir uma nova visita
Author............: Arthur Castaldi
Date..............: 26/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.VisitaInserir(
   17,               -- pUsuarioId
   1,                -- pPeriodo
   1,               -- pAtracao
   1,               -- pTipo
   '1',             -- pNomeResponsavel
   '1',             -- pTelefone
   1,               -- pQtdPessoas
   null,            -- pNomeInstituicao
   '2018-10-28'     -- pData
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;
    vId             Agenda_Ciencia.Visita.id%TYPE;

BEGIN
    INSERT INTO Agenda_Ciencia.Visita (periodo,
                                       atracao,
                                       tipo,
                                       nome_responsavel,
                                       telefone,
                                       qtd_pessoas,
                                       nome_instituicao,
                                       data,
                                       usuario_cadastro)
    VALUES (pPeriodo, pAtracao, pTipo, pNomeResponsavel, pTelefone, pQtdPessoas, pNomeInstituicao, pData, pUsuarioId)
        RETURNING id
            INTO vId;

    RETURN '{"code": 0, "return": {"id": ' || vId || '}}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


/**/