const
    controller = require('./visita.controller');

module.exports = {
    listar: {
        type: 'get',
        path: '/usuario/minha-agenda',
        method: controller.listar,
        public: false,
        admin: false
    },
    listarAdmin: {
        type: 'get',
        path: '/visitas',
        method: controller.listarAdmin,
        public: false,
        admin: true
    },
    inserir: {
        type: 'post',
        path: '/visita',
        method: controller.inserir,
        public: false,
        admin: false
    }
};