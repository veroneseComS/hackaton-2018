const
    repository = require('./visita.repository');

module.exports = {
    listar,
    inserir
};

async function listar(params) {
    return await repository.listar(params);
}

async function inserir(params) {
    const data = await repository.inserir(params);
    return data.return;
}
