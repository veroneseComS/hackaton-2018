const db = global.db;

module.exports = {
    listar,
    inserir
};

async function listar(params) {
    return await db.func('Agenda_Ciencia.VisitaListar', [
        params.usuarioId,
        params.pagina,
        params.linhas
    ])
}

async function inserir(params) {
    return await db.json('Agenda_Ciencia.VisitaInserir', [
        params.usuarioId,
        params.periodo,
        params.atracao,
        params.tipo,
        params.nomeResponsavel,
        params.telefone,
        params.qtdPessoas,
        params.nomeInstituicao,
        params.data
    ])
}
