/**/


SELECT DeleteFunctions('Agenda_Ciencia', 'UsuarioLogin');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.UsuarioLogin(
    pId    INTEGER,
    pEmail Agenda_Ciencia.Usuario.email%TYPE,
    pSenha Agenda_Ciencia.Usuario.senha%TYPE
)
    RETURNS TABLE(
        "id"           Agenda_Ciencia.Usuario.id%TYPE,
        "tipo"         Agenda_Ciencia.Usuario.tipo%TYPE,
        "nome"         Agenda_Ciencia.Usuario.nome%TYPE,
        "email"        Agenda_Ciencia.Usuario.email%TYPE,
        "foto"         TEXT,
        "senhaCorreta" BOOLEAN
    ) AS $$

/*
Documentation
Source file.......: usuario.sql
Description.......: Fazer login com usuário
Author............: Arthur Castaldi
Date..............: 23/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.UsuarioLogin(null, 'arthurcastald@gmail.com', '123'); -- Credentials
SELECT * FROM Agenda_Ciencia.UsuarioLogin(1, null, null); -- Token

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT u.id,
           u.tipo,
           u.nome,
           u.email,
           vS3Bucket || '/usuario/' || u.id || '/' || u.foto foto,
           (u.senha = MD5(pSenha))                           senhaCorreta
    FROM Agenda_Ciencia.Usuario u
    WHERE CASE
              WHEN pId IS NULL
                    THEN u.email = pEmail
              ELSE u.id = pId END;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'UsuarioRegistro');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.UsuarioRegistro(
    pEmail Agenda_Ciencia.Usuario.email%TYPE,
    pSenha Agenda_Ciencia.Usuario.senha%TYPE,
    pNome  Agenda_Ciencia.Usuario.nome%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: usuario.sql
Description.......: Registrar novo usuario
Author............: Arthur Castaldi
Date..............: 24/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.UsuarioRegistro(
   'teste2@teste.com',   -- pEmail
   '123',               -- pSenha
   'Teste'              -- pNome
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;
    vId             Agenda_Ciencia.Usuario.id%TYPE;

BEGIN
    IF EXISTS(SELECT 1 FROM Agenda_Ciencia.Usuario u WHERE u.email = pEmail)
    THEN
        RETURN '{"code": 1, "message": "E-mail existente"}';
    END IF;

    INSERT INTO Agenda_Ciencia.Usuario (email, senha, nome, tipo)
    VALUES (pEmail, MD5(pSenha), pNome, 1)
        RETURNING id
            INTO vId;

    RETURN '{"code": 0, "return": {"id": ' || vId || '}}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'UsuarioAlterarFoto');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.UsuarioAlterarFoto(
    pId   Agenda_Ciencia.Usuario.id%TYPE,
    pFoto Agenda_Ciencia.Usuario.foto%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: usuario.sql
Description.......: Alterar foto do usuario
Author............: Arthur Castaldi
Date..............: 24/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.UsuarioAlterarFoto(
   11,                     -- pId
   'usuario/teste.jpg'     -- pFoto
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    UPDATE Agenda_Ciencia.Usuario SET foto = pFoto WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'UsuarioAlterarSenha');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.UsuarioAlterarSenha(
    pId    Agenda_Ciencia.Usuario.id%TYPE,
    pSenha Agenda_Ciencia.Usuario.senha%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: usuario.sql
Description.......: Alterar senha do usuario
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.UsuarioAlterarSenha(
   17,       -- pId
   '123'     -- pSenha
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    UPDATE Agenda_Ciencia.Usuario SET senha = MD5(pSenha) WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


/**/