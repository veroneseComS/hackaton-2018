const
    config = require('../../config'),
    tokenHelper = require('../../utils/tokenHelper'),
    repository = require('./usuario.repository'),
    s3 = global.s3,
    ALLOWED_EXTENSIONS = ['png', 'jpg'];

module.exports = {
    login,
    registro,
    alterarFoto,
    alterarSenha
};

async function login(params) {
    let usuario;
    let token;

    if (!params.token) {
        usuario = await repository.login(params);
        if (usuario) {
            if (usuario.senhaCorreta) {
                token = await tokenHelper.create({
                    id: usuario.id,
                    tipo: usuario.tipo
                });
            } else {
                throw {httpCode: 403, code: 2, message: 'Senha incorreta'};
            }
        } else {
            throw {httpCode: 403, code: 1, message: 'Usuário não encontrado'};
        }
    } else {
        let tokenRead;
        try {
            tokenRead = await tokenHelper.read(params.token);
        } catch (e) {
            throw {httpCode: 403, code: 3, message: 'Token inválido'}
        }

        usuario = await repository.login({id: tokenRead.id});
        token = await tokenHelper.create({
            id: usuario.id,
            tipo: usuario.tipo
        });
    }

    delete usuario.senhaCorreta;
    delete usuario.id;

    return {
        usuario,
        token
    };
}

async function registro(params) {
    if (params.senha !== params.confirmacaoSenha) {
        throw {httpCode: 400, code: 3, message: 'Confirmação de senha não confere'};
    }

    const data = await repository.registro(params);
    switch (data.code) {
        case 1:
            throw {httpCode: 409, ...data};
    }

    const usuario = await repository.login({id: data.return.id});
    const token = await tokenHelper.create({
        id: usuario.id,
        tipo: usuario.tipo
    });

    delete usuario.senhaCorreta;
    delete usuario.id;

    return {
        usuario,
        token
    };
}

async function alterarFoto(params) {
    const now = new Date().getTime();

    const imageSplit = params.foto.split(';base64,');
    const imageBase64 = imageSplit[1];
    let imageExtension = imageSplit[0].replace('data:image/', '').toLowerCase();
    imageExtension = imageExtension === 'jpeg' ? 'jpg' : imageExtension;
    if (imageExtension === '*;charset=utf-8') {
        imageExtension = 'jpg';
    }
    const fileName = `foto-${now}.${imageExtension}`;
    const filePath = `/usuario/${params.userId}/${fileName}`;

    if (ALLOWED_EXTENSIONS.includes(imageExtension)) {
        try {
            await s3.writeFile(filePath, new Buffer(imageBase64, 'base64'), {
                ACL: 'public-read'
            })
        } catch (err) {
            throw {httpCode: 500, code: 2, message: 'Não foi possível fazer o upload da imagem', err}
        }
    } else {
        throw {httpCode: 400, code: 1, message: 'É permitido imagem apenas com as extensões: png e jpg'}
    }

    await repository.alterarFoto({id: params.userId, foto: fileName});

    return {
        foto: config.s3.url + filePath
    };
}

async function alterarSenha(params) {
    if (params.senha !== params.confirmacaoSenha) {
        throw {httpCode: 400, code: 1, message: 'Confirmação de senha não confere'};
    }

    return await repository.alterarSenha(params);
}
