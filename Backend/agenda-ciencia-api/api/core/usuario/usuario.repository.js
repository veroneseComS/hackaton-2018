const db = global.db;

module.exports = {
    login,
    registro,
    alterarFoto,
    alterarSenha
};

async function login(params) {
    return (await db.func('Agenda_Ciencia.UsuarioLogin', [
        params.id,
        params.email,
        params.senha
    ]))[0];
}

async function registro(params) {
    return await db.json('Agenda_Ciencia.UsuarioRegistro', [
        params.email,
        params.senha,
        params.nome
    ]);
}

async function alterarFoto(params) {
    return await db.json('Agenda_Ciencia.UsuarioAlterarFoto', [
        params.id,
        params.foto
    ]);
}

async function alterarSenha(params) {
    return await db.json('Agenda_Ciencia.UsuarioAlterarSenha', [
        params.userId,
        params.senha
    ]);
}