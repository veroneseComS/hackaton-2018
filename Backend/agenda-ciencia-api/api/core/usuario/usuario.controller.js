const service = require('./usuario.service');

module.exports = {
    login,
    registro,
    alterarFoto,
    alterarSenha
};

async function login(req, res) {
    const params = {
        email: req.body.email,
        senha: req.body.senha,
        token: req.headers.authorization
    };

    const data = await service.login(params);

    res.finish(data)
}

async function registro(req, res) {
    const params = {
        email: req.body.email,
        senha: req.body.senha,
        nome: req.body.nome,
        confirmacaoSenha: req.body.confirmacaoSenha
    };

    const data = await service.registro(params);

    res.finish(data)
}

async function alterarFoto(req, res) {
    const params = {
        userId: req.user.id,
        foto: req.body.foto
    };

    const data = await service.alterarFoto(params);

    res.finish({
        code: 0,
        message: 'Foto alterada com sucesso',
        return: data
    })
}

async function alterarSenha(req, res) {
    const params = {
        userId: req.user.id,
        senha: req.body.senha,
        confirmacaoSenha: req.body.confirmacaoSenha
    };

    await service.alterarSenha(params);

    res.finish({
        code: 0,
        message: 'Senha alterada com sucesso'
    })
}