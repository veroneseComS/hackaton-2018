const
    controller = require('./usuario.controller');

module.exports = {
    login: {
        type: 'post',
        path: '/usuario/login',
        method: controller.login,
        public: true,
        admin: false
    },
    registro: {
        type: 'post',
        path: '/usuario/registro',
        method: controller.registro,
        public: true,
        admin: false
    },
    alterarFoto: {
        type: 'put',
        path: '/usuario/alterar-foto',
        method: controller.alterarFoto,
        public: false,
        admin: false
    },
    alterarSenha: {
        type: 'put',
        path: '/usuario/alterar-senha',
        method: controller.alterarSenha,
        public: false,
        admin: false
    }
};