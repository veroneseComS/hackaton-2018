const service = require('./mural.service');

module.exports = {
    listar,
    inserir,
    curtir
};

async function listar(req, res) {
    const params = {
        usuarioId: req.user.id,
        pagina: req.query.pagina,
        linhas: req.query.linhas
    };

    const data = await service.listar(params);

    res.finish(data)
}

async function inserir(req, res) {
    const params = {
        usuarioId: req.user.id,
        descricao: req.body.descricao,
        novaImagem: req.body.novaImagem
    };

    const data = await service.inserir(params);

    res.finish({
        code: 0,
        message: 'Foto adicionada',
        return: data
    })
}

async function curtir(req, res) {
    const params = {
        usuarioId: req.user.id,
        muralId: req.params.id,
        curtir: req.body.curtir
    };

    await service.curtir(params);

    res.finish({
        code: 0,
        message: 'Foto ' + (params.curtir ? 'curtida' : 'descurtida')
    })
}