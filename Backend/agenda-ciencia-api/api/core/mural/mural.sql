/**/


SELECT DeleteFunctions('Agenda_Ciencia', 'MuralListar');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.MuralListar(
    pUsuarioId Agenda_Ciencia.Usuario.id%TYPE,
    pPagina    INTEGER,
    pLinhas    INTEGER
)
    RETURNS TABLE(
        "totalLinhas"     BIGINT,
        "id"              Agenda_Ciencia.Mural.id%TYPE,
        "imagem"          TEXT,
        "descricao"       Agenda_Ciencia.Mural.descricao%TYPE,
        "usuarioCadastro" JSON,
        "dataCadastro"    Agenda_Ciencia.Mural.data_cadastro%TYPE,
        "curtidas"        BIGINT,
        "euCurti"         BOOLEAN
    ) AS $$

/*
Documentation
Source file.......: mural.sql
Description.......: Listar mural
Author............: Arthur Castaldi
Date..............: 23/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.MuralListar(17, null, null);

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT COUNT(1) OVER (PARTITION BY 1)                                                  totalLinhas,
           m.id,
           vS3Bucket || '/mural/' || m.id || '/' || m.imagem                               imagem,
           m.descricao,
           ('{"nome": ' || (COALESCE('"' || u.nome || '"', 'null')) || ', "foto": ' ||
            (COALESCE('"' || vS3Bucket || '/usuario/' || u.id || '/' || u.foto || '"', 'null')) || '}') :: JSON,
           m.data_cadastro,
           (SELECT COUNT(1) FROM Agenda_Ciencia.Mural_Curtida mc WHERE mc.mural_id = m.id) curtidas,
           (SELECT COUNT(1) > 0
            FROM Agenda_Ciencia.Mural_Curtida mc
            WHERE mc.mural_id = m.id
              AND mc.usuario_id = pUsuarioId)                                              euCurti
    FROM Agenda_Ciencia.Mural m
             INNER JOIN Agenda_Ciencia.Usuario u ON u.id = m.usuario_cadastro
    ORDER BY m.data_cadastro DESC
    LIMIT iif(pLinhas > 0 AND pPagina > 0, pLinhas, NULL)
    OFFSET iif(pLinhas > 0 AND pPagina > 0, (pPagina - 1) * pLinhas, NULL);
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'MuralInserir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.MuralInserir(
    pUsuarioId Agenda_Ciencia.Usuario.id%TYPE,
    pDescricao Agenda_Ciencia.Mural.descricao%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: mural.sql
Description.......: Inserir uma nova foto no mural
Author............: Arthur Castaldi
Date..............: 26/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.MuralInserir(
   1,               -- pUsuarioId
   'Mural Test',    -- pDescricao
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;
    vId             Agenda_Ciencia.Mural.id%TYPE;

BEGIN
    INSERT INTO Agenda_Ciencia.Mural (descricao, usuario_cadastro)
    VALUES (pDescricao, pUsuarioId)
        RETURNING id
            INTO vId;

    RETURN '{"code": 0, "return": {"id": ' || vId || '}}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'MuralAlterarImagem');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.MuralAlterarImagem(
    pId     Agenda_Ciencia.Mural.id%TYPE,
    pImagem Agenda_Ciencia.Mural.imagem%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: mural.sql
Description.......: Alterar imagem do mural
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.MuralAlterarImagem(
   1,                     -- pId
   'noticia/teste.jpg'    -- pImagem
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    UPDATE Agenda_Ciencia.Mural SET imagem = pImagem WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'MuralCurtir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.MuralCurtir(
    pUsuarioId Agenda_Ciencia.Mural_Curtida.usuario_id%TYPE,
    pMuralId   Agenda_Ciencia.Mural_Curtida.mural_id%TYPE,
    pCurtir    BOOLEAN
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: mural.sql
Description.......: Curtir foto no mural
Author............: Arthur Castaldi
Date..............: 27/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.MuralCurtir(
   17,    -- pUsuarioId
   3,     -- pMuralId,
   true  -- pCurtir
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
--     RAISE EXCEPTION '%, %, %', pUsuarioId, pMuralId, pCurtir;

    IF NOT EXISTS(SELECT 1 FROM Agenda_Ciencia.Mural m WHERE m.id = pMuralId)
    THEN
        RETURN '{"code": 1, "message": "Foto não encontrada"}';
    END IF;

    IF pCurtir AND NOT EXISTS(SELECT 1 FROM Agenda_Ciencia.Mural_Curtida WHERE usuario_id = pUsuarioId
                                                                           AND mural_id = pMuralId)
    THEN
        INSERT INTO Agenda_Ciencia.Mural_Curtida (usuario_id, mural_id) VALUES (pUsuarioId, pMuralId);
    ELSE
        DELETE FROM Agenda_Ciencia.Mural_Curtida WHERE usuario_id = pUsuarioId
                                                   AND mural_id = pMuralId;
    END IF;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


/**/