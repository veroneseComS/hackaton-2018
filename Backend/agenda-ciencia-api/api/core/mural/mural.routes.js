const
    controller = require('./mural.controller');

module.exports = {
    listar: {
        type: 'get',
        path: '/mural',
        method: controller.listar,
        public: false,
        admin: false
    },
    inserir: {
        type: 'post',
        path: '/mural',
        method: controller.inserir,
        public: false,
        admin: true
    },
    curtir: {
        type: 'post',
        path: '/mural/:id/curtir',
        method: controller.curtir,
        public: false,
        admin: false
    }
};