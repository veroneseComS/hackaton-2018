const db = global.db;

module.exports = {
    listar,
    inserir,
    alterarImagem,
    curtir
};

async function listar(params) {
    return await db.func('Agenda_Ciencia.MuralListar', [
        params.usuarioId,
        params.pagina,
        params.linhas
    ]);
}

async function inserir(params) {
    return await db.json('Agenda_Ciencia.MuralInserir', [
        params.usuarioId,
        params.descricao
    ]);
}

async function alterarImagem(params) {
    return await db.json('Agenda_Ciencia.MuralAlterarImagem', [
        params.id,
        params.imagem
    ]);
}

async function curtir(params) {
    return await db.json('Agenda_Ciencia.MuralCurtir', [
        params.usuarioId,
        params.muralId,
        params.curtir
    ]);
}