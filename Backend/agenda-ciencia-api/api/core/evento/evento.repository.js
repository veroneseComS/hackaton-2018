const db = global.db;

module.exports = {
    listar,
    inserir,
    alterarImagem
};

async function listar(params) {
    return await db.func('Agenda_Ciencia.EventoListar', [
        params.ano,
        params.dia
    ])
}

async function inserir(params) {
    return await db.json('Agenda_Ciencia.EventoInserir', [
        params.usuarioId,
        params.data,
        params.titulo,
        params.descricao,
        params.sobre,
        params.valor,
        params.vagas,
        params.horaInicio,
        params.horaFinal
    ])
}

async function alterarImagem(params) {
    return await db.json('Agenda_Ciencia.EventoAlterarImagem', [
        params.id,
        params.imagem
    ])
}