const
    controller = require('./evento.controller');

module.exports = {
    listar: {
        type: 'get',
        path: '/eventos',
        method: controller.listar,
        public: false,
        admin: false
    },
    inserir: {
        type: 'post',
        path: '/evento',
        method: controller.inserir,
        public: false,
        admin: true
    }
};