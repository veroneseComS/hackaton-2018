const
    repository = require('./evento.repository'),
    s3 = global.s3,
    ALLOWED_EXTENSIONS = ['png', 'jpg'];

module.exports = {
    listar,
    inserir
};

async function listar(params) {
    return await repository.listar(params);
}

async function inserir(params) {
    const data = await repository.inserir(params);

    params.id = data.return.id;
    if (params.novaImagem) {
        await alterarImagem(params);
    }

    return data.return;
}

async function alterarImagem(params) {
    const now = new Date().getTime();

    const imageSplit = params.novaImagem.split(';base64,');
    const imageBase64 = imageSplit[1];
    let imageExtension = imageSplit[0].replace('data:image/', '').toLowerCase();
    imageExtension = imageExtension === 'jpeg' ? 'jpg' : imageExtension;
    if (imageExtension === '*;charset=utf-8') {
        imageExtension = 'jpg';
    }
    const fileName = `imagem-${now}.${imageExtension}`;
    const filePath = `/evento/${params.id}/${fileName}`;

    if (ALLOWED_EXTENSIONS.includes(imageExtension)) {
        try {
            await s3.writeFile(filePath, new Buffer(imageBase64, 'base64'), {
                ACL: 'public-read'
            })
        } catch (err) {
            throw {httpCode: 500, code: 2, message: 'Não foi possível fazer o upload da imagem', err}
        }
    } else {
        throw {httpCode: 400, code: 1, message: 'É permitido imagem apenas com as extensões: png e jpg'}
    }

    await repository.alterarImagem({id: params.id, imagem: fileName});
}