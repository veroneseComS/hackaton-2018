/**/


SELECT DeleteFunctions('Agenda_Ciencia', 'EventoListar');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.EventoListar(
    pAno INTEGER,
    pDia DATE
)
    RETURNS TABLE(
        "id"              Agenda_Ciencia.Evento.id%TYPE,
        "descricaoEvento" Agenda_Ciencia.Evento.descricao%TYPE,
        "year"            DOUBLE PRECISION,
        "month"           DOUBLE PRECISION,
        "date"            DOUBLE PRECISION,
        "horaInicio"      Agenda_Ciencia.Evento.hora_inicio%TYPE,
        "horaFinal"       Agenda_Ciencia.Evento.hora_final%TYPE,
        "titulo"          Agenda_Ciencia.Evento.titulo%TYPE,
        "sobre"           Agenda_Ciencia.Evento.sobre%TYPE,
        "valor"           Agenda_Ciencia.Evento.valor%TYPE,
        "vagas"           Agenda_Ciencia.Evento.vagas%TYPE,
        "imagem"          TEXT
    ) AS $$

/*
Documentation
Source file.......: evento.sql
Description.......: Listar evento
Author............: Arthur Castaldi
Date..............: 24/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.EventoListar(2018, null);

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT e.id,
           e.descricao,
           EXTRACT(year from e.data)                          "ano",
           EXTRACT(month from e.data) - 1                     "mes",
           EXTRACT(day from e.data)                           "dia",
           e.hora_inicio,
           e.hora_final,
           e.titulo,
           e.sobre,
           e.valor,
           e.vagas,
           vS3Bucket || '/evento/' || e.id || '/' || e.imagem imagem
    FROM Agenda_Ciencia.Evento e
    WHERE (EXTRACT(year from e.data) = pAno OR e.data = pDia)
      AND ((e.data || ' ' || e.hora_final || ' -3') :: TIMESTAMP WITH TIME ZONE) >= CURRENT_TIMESTAMP
    ORDER BY e.data, e.hora_inicio;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'EventoInserir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.EventoInserir(
    pUsuarioId  Agenda_Ciencia.Usuario.id%TYPE,
    pData       Agenda_Ciencia.Evento.data%TYPE,
    pTitulo     Agenda_Ciencia.Evento.titulo%TYPE,
    pDescricao  Agenda_Ciencia.Evento.descricao%TYPE,
    pSobre      Agenda_Ciencia.Evento.sobre%TYPE,
    pValor      Agenda_Ciencia.Evento.valor%TYPE,
    pVagas      INTEGER,
    pHoraInicio Agenda_Ciencia.Evento.hora_inicio%TYPE,
    pHoraFinal  Agenda_Ciencia.Evento.hora_final%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: evento.sql
Description.......: Inserir um novo evento
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.EventoInserir(
   1,               -- pUsuarioId
   '2018-10-25',    -- pData
   '1',             -- pTitulo
   '1',             -- pDescricao
   '1',             -- pSobre
   1,               -- pValor
   1,               -- pVagas
   '20:00:00',      -- pHoraInicio
   '23:00:00'       -- pHoraFinal
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;
    vId             Agenda_Ciencia.Evento.id%TYPE;

BEGIN
    INSERT INTO Agenda_Ciencia.Evento (data,
                                       titulo,
                                       descricao,
                                       sobre,
                                       valor,
                                       vagas,
                                       hora_inicio,
                                       hora_final,
                                       usuario_cadastro)
    VALUES (pData, pTitulo, pDescricao, pSobre, pValor, pVagas, pHoraInicio, pHoraFinal, pUsuarioId)
        RETURNING id
            INTO vId;

    RETURN '{"code": 0, "return": {"id": ' || vId || '}}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'EventoAlterarImagem');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.EventoAlterarImagem(
    pId     Agenda_Ciencia.Evento.id%TYPE,
    pImagem Agenda_Ciencia.Evento.imagem%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: evento.sql
Description.......: Alterar imagem do evento
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.EventoAlterarImagem(
   1,                     -- pId
   'evento/teste.jpg'     -- pImagem
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    UPDATE Agenda_Ciencia.Evento SET imagem = pImagem WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


/**/