const service = require('./evento.service');

module.exports = {
    listar,
    inserir
};

async function listar(req, res) {
    const params = {
        ano: req.query.ano,
        dia: req.query.dia
    };

    const data = await service.listar(params);

    res.finish(data)
}

async function inserir(req, res) {
    const params = {
        usuarioId: req.user.id,
        data: req.body.data,
        titulo: req.body.titulo,
        descricao: req.body.descricao,
        sobre: req.body.sobre,
        valor: req.body.valor,
        vagas: req.body.vagas,
        horaInicio: req.body.horaInicio,
        horaFinal: req.body.horaFinal,
        novaImagem: req.body.novaImagem
    };

    const data = await service.inserir(params);

    res.finish({
        code: 0,
        message: 'Evento adicionado',
        return: data
    })
}