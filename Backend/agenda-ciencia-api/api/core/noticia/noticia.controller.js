const
    service = require('./noticia.service');

module.exports = {
    listar,
    listarPorId,
    inserir,
    atualizar,
    excluir
};

async function listar(req, res) {
    const params = {
        pagina: req.query.pagina,
        linhas: req.query.linhas
    };

    const data = await service.listar(params);

    res.finish(data)
}

async function listarPorId(req, res) {
    const params = {
        id: req.params.id
    };

    const data = await service.listarPorId(params);

    res.finish(data)
}

async function inserir(req, res) {
    const params = {
        usuarioId: req.user.id,
        titulo: req.body.titulo,
        subtitulo: req.body.subtitulo,
        texto: req.body.texto,
        novaImagem: req.body.novaImagem
    };

    const data = await service.inserir(params);

    res.finish({
        code: 0,
        message: 'Notícia adicionada',
        return: data
    })
}

async function atualizar(req, res) {
    const params = {
        usuarioId: req.user.id,
        id: req.params.id,
        titulo: req.body.titulo,
        subtitulo: req.body.subtitulo,
        texto: req.body.texto,
        novaImagem: req.body.novaImagem
    };

    await service.atualizar(params);

    res.finish({
        code: 0,
        message: 'Notícia atualizada'
    })
}

async function excluir(req, res) {
    const params = {
        id: req.params.id
    };

    await service.excluir(params);

    res.finish({
        code: 0,
        message: 'Notícia excluída'
    })
}