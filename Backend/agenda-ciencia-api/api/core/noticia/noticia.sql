/**/


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaListar');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaListar(
    pPagina INTEGER,
    pLinhas INTEGER
)
    RETURNS TABLE(
        "totalLinhas" BIGINT,
        "id"          Agenda_Ciencia.Noticia.id%TYPE,
        "imagem"      TEXT,
        "titulo"      Agenda_Ciencia.Noticia.titulo%TYPE,
        "subtitulo"   Agenda_Ciencia.Noticia.subtitulo%TYPE
    ) AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Listar notícias
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaListar(1, 10);

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT COUNT(1) OVER (PARTITION BY 1)                      totalLinhas,
           n.id,
           vS3Bucket || '/noticia/' || n.id || '/' || n.imagem imagem,
           n.titulo,
           n.subtitulo
    FROM Agenda_Ciencia.Noticia n
    ORDER BY n.data_cadastro DESC
    LIMIT iif(pLinhas > 0 AND pPagina >= 0, pLinhas, NULL)
    OFFSET iif(pLinhas > 0 AND pPagina >= 0, (pPagina - 1) * pLinhas, NULL);
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaListarPorId');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaListarPorId(
    pId Agenda_Ciencia.Noticia.id%TYPE
)
    RETURNS TABLE(
        "id"              Agenda_Ciencia.Noticia.id%TYPE,
        "imagem"          TEXT,
        "titulo"          Agenda_Ciencia.Noticia.titulo%TYPE,
        "subtitulo"       Agenda_Ciencia.Noticia.subtitulo%TYPE,
        "texto"           Agenda_Ciencia.Noticia.texto%TYPE,
        "usuarioCadastro" JSON,
        "dataCadastro"    Agenda_Ciencia.Noticia.data_cadastro%TYPE
    ) AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Listar uma noticia por ID
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaListarPorId(1);

*/

DECLARE
    vS3Bucket Agenda_Ciencia.Configuracao.s3Bucket%TYPE;

BEGIN
    vS3Bucket = (SELECT s.s3Bucket FROM Agenda_Ciencia.Configuracao s LIMIT 1);

    RETURN QUERY
    SELECT n.id,
           vS3Bucket || '/noticia/' || n.id || '/' || n.imagem imagem,
           n.titulo,
           n.subtitulo,
           n.texto,
           ('{"nome": ' || (COALESCE('"' || u.nome || '"', 'null')) || ', "foto": ' ||
            (COALESCE('"' || vS3Bucket || '/usuario/' || u.id || '/' || u.foto || '"', 'null')) || '}') :: JSON,
           n.data_cadastro
    FROM Agenda_Ciencia.Noticia n
             INNER JOIN Agenda_Ciencia.Usuario u ON u.id = n.usuario_cadastro
    WHERE n.id = pId;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaInserir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaInserir(
    pUsuarioId Agenda_Ciencia.Usuario.id%TYPE,
    pTitulo    Agenda_Ciencia.Noticia.titulo%TYPE,
    pSubtitulo Agenda_Ciencia.Noticia.subtitulo%TYPE,
    pTexto     Agenda_Ciencia.Noticia.texto%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Inserir uma nova noticia
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaInserir(
   1,               -- pUsuarioId
   '1'              -- pTitulo
   '1'              -- pSubtitulo
   '1'              -- pTexto
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;
    vId             Agenda_Ciencia.Noticia.id%TYPE;

BEGIN
    INSERT INTO Agenda_Ciencia.Noticia (titulo, subtitulo, texto, usuario_cadastro)
    VALUES (pTitulo, pSubtitulo, pTexto, pUsuarioId)
        RETURNING id
            INTO vId;

    RETURN '{"code": 0, "return": {"id": ' || vId || '}}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaAlterarImagem');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaAlterarImagem(
    pId     Agenda_Ciencia.Noticia.id%TYPE,
    pImagem Agenda_Ciencia.Noticia.imagem%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Alterar imagem da noticia
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaAlterarImagem(
   1,                     -- pId
   'noticia/teste.jpg'    -- pImagem
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    UPDATE Agenda_Ciencia.Noticia SET imagem = pImagem WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaAtualizar');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaAtualizar(
    pUsuarioId Agenda_Ciencia.Usuario.id%TYPE,
    pId        Agenda_Ciencia.Noticia.id%TYPE,
    pTitulo    Agenda_Ciencia.Noticia.titulo%TYPE,
    pSubtitulo Agenda_Ciencia.Noticia.subtitulo%TYPE,
    pTexto     Agenda_Ciencia.Noticia.texto%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Atualizar a noticia
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaAtualizar(
   17,              -- pUsuarioId
   1,               -- pId
   'Noticia Test',  -- pTitulo
   '1',             -- pSubtitulo,
   '1'              -- pTexto
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    IF NOT EXISTS(SELECT 1 FROM Agenda_Ciencia.Noticia p WHERE p.id = pId)
    THEN
        RETURN '{"code": 1, "message": "Notícia não encontrada"}';
    END IF;

    UPDATE Agenda_Ciencia.Noticia
    SET titulo            = pTitulo,
        subtitulo         = pSubtitulo,
        texto             = pTexto,
        usuario_alteracao = pUsuarioId,
        data_alteracao    = CURRENT_TIMESTAMP
    WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


SELECT DeleteFunctions('Agenda_Ciencia', 'NoticiaExcluir');
CREATE OR REPLACE FUNCTION Agenda_Ciencia.NoticiaExcluir(
    pId Agenda_Ciencia.Noticia.id%TYPE
)
    RETURNS JSON AS $$

/*
Documentation
Source file.......: noticia.sql
Description.......: Excluir a noticia
Author............: Arthur Castaldi
Date..............: 25/10/2018
Ex................:

SELECT * FROM Agenda_Ciencia.NoticiaExcluir(
   2   -- pId
);

*/

DECLARE
    vErrorProcedure TEXT;
    vErrorMessage   TEXT;

BEGIN
    IF NOT EXISTS(SELECT 1 FROM Agenda_Ciencia.Noticia c WHERE c.id = pId)
    THEN
        RETURN '{"code": 1, "message": "Notícia não encontrada"}';
    END IF;

    DELETE FROM Agenda_Ciencia.Noticia WHERE id = pId;

    RETURN '{"code": 0}';
    EXCEPTION WHEN OTHERS
    THEN
        GET STACKED DIAGNOSTICS vErrorProcedure = MESSAGE_TEXT;
        GET STACKED DIAGNOSTICS vErrorMessage = PG_EXCEPTION_CONTEXT;
        RAISE EXCEPTION 'Internal Error: (%) %', vErrorProcedure, vErrorMessage;
END;
$$
LANGUAGE plpgsql;


/**/