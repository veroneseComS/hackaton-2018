const
    controller = require('./noticia.controller');

module.exports = {
    listar: {
        type: 'get',
        path: '/noticias',
        method: controller.listar,
        public: false,
        admin: false
    },
    listarPorId: {
        type: 'get',
        path: '/noticia/:id',
        method: controller.listarPorId,
        public: false,
        admin: false
    },
    inserir: {
        type: 'post',
        path: '/noticia',
        method: controller.inserir,
        public: false,
        admin: true
    },
    atualizar: {
        type: 'put',
        path: '/noticia/:id',
        method: controller.atualizar,
        public: false,
        admin: true
    },
    excluir: {
        type: 'delete',
        path: '/noticia/:id',
        method: controller.excluir,
        public: false,
        admin: true
    }
};