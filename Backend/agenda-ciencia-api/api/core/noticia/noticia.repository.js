const db = global.db;

module.exports = {
    listar,
    listarPorId,
    inserir,
    atualizar,
    alterarImagem,
    excluir
};

async function listar(params) {
    return await db.func('Agenda_Ciencia.NoticiaListar', [
        params.pagina,
        params.linhas
    ]);
}

async function listarPorId(params) {
    return (await db.func('Agenda_Ciencia.NoticiaListarPorId', [
        params.id
    ]))[0];
}

async function inserir(params) {
    return await db.json('Agenda_Ciencia.NoticiaInserir', [
        params.usuarioId,
        params.titulo,
        params.subtitulo,
        params.texto
    ]);
}

async function atualizar(params) {
    return await db.json('Agenda_Ciencia.NoticiaAtualizar', [
        params.usuarioId,
        params.id,
        params.titulo,
        params.subtitulo,
        params.texto
    ]);
}

async function alterarImagem(params) {
    return await db.json('Agenda_Ciencia.NoticiaAlterarImagem', [
        params.id,
        params.imagem
    ]);
}

async function excluir(params) {
    return await db.json('Agenda_Ciencia.NoticiaExcluir', [
        params.id
    ]);
}