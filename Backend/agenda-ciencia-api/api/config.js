const config = {
    env: process.env.NODE_ENV || 'dev',
    debug: process.env.DEBUG === 'true',
    clusterCount: process.env.CLUSTER_COUNT || require('os').cpus().length,
    host: process.env.HOST || 'localhost',
    port: process.env.PORT || 3000,
    db: {
        host: process.env.PGHOST || 'localhost',
        port: process.env.PGPORT || 5432,
        database: process.env.PGDATABASE || 'agenda_ciencia',
        user: process.env.PGUSER || 'vermelho',
        password: process.env.PGPASSWORD || '123',
    },
    hash: process.env.HASH || 'facef',
    s3: {
        region: process.env.S3REGION || 'us-east-1',
        bucket: process.env.S3BUCKET || 'agenda-ciencia-dev',
        accessKeyId: process.env.S3ACCESSKEYID,
        secretAccessKey: process.env.S3SECRETACCESSKEY
    }
};
config.s3.url = 'https://s3.amazonaws.com/' + config.s3.bucket;

module.exports = config;
