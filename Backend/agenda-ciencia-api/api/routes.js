const asyncMiddleware = require('./utils/asyncMiddleware');

module.exports = app => {
    const functionalities = {
        ping: require('./core/ping/ping.routes'),
        usuario: require('./core/usuario/usuario.routes'),
        mural: require('./core/mural/mural.routes'),
        evento: require('./core/evento/evento.routes'),
        noticia: require('./core/noticia/noticia.routes'),
        visita: require('./core/visita/visita.routes')
    };

    for (const functionalityKey in functionalities) {
        for (const methodKey in functionalities[functionalityKey]) {
            if (functionalities[functionalityKey].hasOwnProperty(methodKey)) {
                const route = functionalities[functionalityKey][methodKey];
                app[route.type](route.path, asyncMiddleware(route.method, route.public, route.admin))
            }
        }
    }

    app.get('/api', asyncMiddleware((req, res) => {
        res.send(functionalities)
    }, true))
};