-- Diff code generated with pgModeler (PostgreSQL Database Modeler)
-- pgModeler version: 0.9.2-alpha
-- Diff date: 2018-10-26 11:22:28
-- Source model: agenda_ciencia
-- Database: agenda_ciencia
-- PostgreSQL version: 9.2

-- [ Diff summary ]
-- Dropped objects: 57
-- Created objects: 39
-- Changed objects: 0
-- Truncated tables: 0

SET search_path=public,pg_catalog,agenda_ciencia;
-- ddl-end --


-- [ Dropped objects ] --
ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_usuario_cadastro_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_tipo_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_atracao_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_periodo_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_pk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.mural_curtida DROP CONSTRAINT IF EXISTS mural_curtida_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.mural_curtida DROP CONSTRAINT IF EXISTS mural_curtida_mural_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.mural DROP CONSTRAINT IF EXISTS mural_usuario_cadastro_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_usuario_alteracao_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_usuario_cadastro_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_usuario_alteracao_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_usuario_cadastro_usuario_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.usuario DROP CONSTRAINT IF EXISTS usuario_tipo_id_fk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.mural DROP CONSTRAINT IF EXISTS mural_pk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_pk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.usuario DROP CONSTRAINT IF EXISTS usuario_pk CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_pk CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.visitainserir(integer,integer,integer,integer,character varying,character varying,integer,character varying,date) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.visitalistar(IN integer,IN integer,IN integer) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.eventolistar(IN integer,IN integer) CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.visita DROP COLUMN IF EXISTS id CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.visita CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.tipo_visita CASCADE;
-- ddl-end --
DROP SEQUENCE IF EXISTS agenda_ciencia.visita_id_seq CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.atracao CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.periodo CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.eventoalterarimagem(integer,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.eventoinserir(integer,date,character varying,character varying,character varying,numeric,integer,time with time zone,time with time zone) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticiaexcluir(integer) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticiaatualizar(integer,integer,character varying,character varying,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticiaalterarimagem(integer,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticiainserir(integer,character varying,character varying,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticialistar(IN integer,IN integer) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.usuarioalterarsenha(integer,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.noticialistarporid(IN integer) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.murallistar(IN integer,IN integer,IN integer) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.usuarioalterarfoto(integer,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.usuarioregistro(character varying,character varying,character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS agenda_ciencia.usuariologin(IN integer,IN character varying,IN character varying) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS public.deletefunctions(text,text) CASCADE;
-- ddl-end --
DROP FUNCTION IF EXISTS public.iif(boolean,anyelement,anyelement) CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.configuracao CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.tipo_usuario CASCADE;
-- ddl-end --
DROP INDEX IF EXISTS agenda_ciencia.usuario_email_idx CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.mural_curtida CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.mural DROP COLUMN IF EXISTS id CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.mural CASCADE;
-- ddl-end --
DROP SEQUENCE IF EXISTS agenda_ciencia.mural_id_seq CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.noticia DROP COLUMN IF EXISTS id CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.noticia CASCADE;
-- ddl-end --
DROP SEQUENCE IF EXISTS agenda_ciencia.noticia_id_seq CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.usuario DROP COLUMN IF EXISTS id CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.usuario CASCADE;
-- ddl-end --
ALTER TABLE agenda_ciencia.evento DROP COLUMN IF EXISTS id CASCADE;
-- ddl-end --
DROP TABLE IF EXISTS agenda_ciencia.evento CASCADE;
-- ddl-end --
DROP SEQUENCE IF EXISTS agenda_ciencia.evento_id_seq CASCADE;
-- ddl-end --
DROP SEQUENCE IF EXISTS agenda_ciencia.usuario_id_seq CASCADE;
-- ddl-end --


-- [ Created objects ] --
-- object: agenda_ciencia.usuario_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS agenda_ciencia.usuario_id_seq CASCADE;
CREATE SEQUENCE agenda_ciencia.usuario_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --

-- object: agenda_ciencia.evento_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS agenda_ciencia.evento_id_seq CASCADE;
CREATE SEQUENCE agenda_ciencia.evento_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --

-- object: agenda_ciencia.evento | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.evento CASCADE;
CREATE TABLE agenda_ciencia.evento(
	id integer NOT NULL DEFAULT nextval('agenda_ciencia.evento_id_seq'::regclass),
	imagem varchar(40),
	data date NOT NULL,
	hora_inicio time with time zone NOT NULL,
	hora_final time with time zone NOT NULL,
	titulo varchar(40) NOT NULL,
	descricao varchar(255) NOT NULL,
	sobre varchar(255) NOT NULL,
	valor decimal(10,2) NOT NULL,
	vagas smallint NOT NULL,
	usuario_cadastro integer NOT NULL,
	data_cadastro timestamp with time zone NOT NULL DEFAULT now(),
	usuario_alteracao integer,
	data_alteracao timestamp with time zone,
	CONSTRAINT evento_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.evento OWNER TO postgres;
-- ddl-end --

-- object: agenda_ciencia.usuario | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.usuario CASCADE;
CREATE TABLE agenda_ciencia.usuario(
	id integer NOT NULL DEFAULT nextval('agenda_ciencia.usuario_id_seq'::regclass),
	tipo smallint NOT NULL,
	email varchar(255) NOT NULL,
	senha varchar(100) NOT NULL,
	nome varchar(60),
	foto varchar(40),
	CONSTRAINT usuario_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.usuario OWNER TO postgres;
-- ddl-end --

-- object: agenda_ciencia.noticia_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS agenda_ciencia.noticia_id_seq CASCADE;
CREATE SEQUENCE agenda_ciencia.noticia_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --

-- object: agenda_ciencia.noticia | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.noticia CASCADE;
CREATE TABLE agenda_ciencia.noticia(
	id integer NOT NULL DEFAULT nextval('agenda_ciencia.noticia_id_seq'::regclass),
	imagem varchar(40),
	titulo varchar(40) NOT NULL,
	subtitulo varchar(40) NOT NULL,
	texto varchar(255) NOT NULL,
	usuario_cadastro integer NOT NULL,
	data_cadastro timestamp with time zone NOT NULL DEFAULT now(),
	usuario_alteracao integer,
	data_alteracao timestamp with time zone,
	CONSTRAINT noticia_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.noticia OWNER TO postgres;
-- ddl-end --

-- object: agenda_ciencia.mural_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS agenda_ciencia.mural_id_seq CASCADE;
CREATE SEQUENCE agenda_ciencia.mural_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --

-- object: agenda_ciencia.mural | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.mural CASCADE;
CREATE TABLE agenda_ciencia.mural(
	id integer NOT NULL DEFAULT nextval('agenda_ciencia.mural_id_seq'::regclass),
	imagem varchar(40),
	descricao varchar(100),
	usuario_cadastro integer NOT NULL,
	data_cadastro timestamp with time zone NOT NULL DEFAULT now(),
	CONSTRAINT mural_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.mural OWNER TO postgres;
-- ddl-end --

-- object: agenda_ciencia.mural_curtida | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.mural_curtida CASCADE;
CREATE TABLE agenda_ciencia.mural_curtida(
	mural_id integer NOT NULL,
	usuario_id integer NOT NULL,
	CONSTRAINT mural_curtida_pk PRIMARY KEY (mural_id,usuario_id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.mural_curtida OWNER TO postgres;
-- ddl-end --

-- object: usuario_email_idx | type: INDEX --
-- DROP INDEX IF EXISTS agenda_ciencia.usuario_email_idx CASCADE;
CREATE INDEX usuario_email_idx ON agenda_ciencia.usuario
	USING btree
	(
	  email
	);
-- ddl-end --

-- object: agenda_ciencia.tipo_usuario | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.tipo_usuario CASCADE;
CREATE TABLE agenda_ciencia.tipo_usuario(
	id smallint NOT NULL,
	nome varchar(15) NOT NULL,
	CONSTRAINT tipo_usuario_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.tipo_usuario OWNER TO postgres;
-- ddl-end --

INSERT INTO agenda_ciencia.tipo_usuario (id, nome) VALUES (E'1', E'Usuário');
-- ddl-end --
INSERT INTO agenda_ciencia.tipo_usuario (id, nome) VALUES (E'2', E'Administrador');
-- ddl-end --

-- object: agenda_ciencia.configuracao | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.configuracao CASCADE;
CREATE TABLE agenda_ciencia.configuracao(
	s3bucket varchar(100)
);
-- ddl-end --
ALTER TABLE agenda_ciencia.configuracao OWNER TO postgres;
-- ddl-end --

INSERT INTO agenda_ciencia.configuracao (s3bucket) VALUES (E'''https://s3.amazonaws.com/agenda-ciencia/''');
-- ddl-end --

-- object: agenda_ciencia.periodo | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.periodo CASCADE;
CREATE TABLE agenda_ciencia.periodo(
	id smallint NOT NULL,
	nome varchar(10),
	CONSTRAINT periodo_visita_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.periodo OWNER TO postgres;
-- ddl-end --

INSERT INTO agenda_ciencia.periodo (id, nome) VALUES (E'1', E'Manhã');
-- ddl-end --
INSERT INTO agenda_ciencia.periodo (id, nome) VALUES (E'2', E'Tarde');
-- ddl-end --
INSERT INTO agenda_ciencia.periodo (id, nome) VALUES (E'3', E'Noite');
-- ddl-end --

-- object: agenda_ciencia.atracao | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.atracao CASCADE;
CREATE TABLE agenda_ciencia.atracao(
	id smallint NOT NULL,
	nome varchar(30),
	CONSTRAINT atracao_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.atracao OWNER TO postgres;
-- ddl-end --

INSERT INTO agenda_ciencia.atracao (id, nome) VALUES (E'1', E'Observatório');
-- ddl-end --
INSERT INTO agenda_ciencia.atracao (id, nome) VALUES (E'2', E'Museu Interativo de Ciências');
-- ddl-end --
INSERT INTO agenda_ciencia.atracao (id, nome) VALUES (E'3', E'Ciência Móvel');
-- ddl-end --

-- object: agenda_ciencia.visita_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS agenda_ciencia.visita_id_seq CASCADE;
CREATE SEQUENCE agenda_ciencia.visita_id_seq
	INCREMENT BY 1
	MINVALUE -2147483648
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --

-- object: agenda_ciencia.tipo_visita | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.tipo_visita CASCADE;
CREATE TABLE agenda_ciencia.tipo_visita(
	id smallint NOT NULL,
	nome varchar(10) NOT NULL,
	CONSTRAINT tipo_visita_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.tipo_visita OWNER TO postgres;
-- ddl-end --

INSERT INTO agenda_ciencia.tipo_visita (id, nome) VALUES (E'1', E'Escolar');
-- ddl-end --
INSERT INTO agenda_ciencia.tipo_visita (id, nome) VALUES (E'2', E'Comum');
-- ddl-end --

-- object: agenda_ciencia.visita | type: TABLE --
-- DROP TABLE IF EXISTS agenda_ciencia.visita CASCADE;
CREATE TABLE agenda_ciencia.visita(
	id integer NOT NULL DEFAULT nextval('agenda_ciencia.visita_id_seq'::regclass),
	periodo smallint NOT NULL,
	atracao smallint NOT NULL,
	tipo smallint NOT NULL,
	data date NOT NULL,
	nome_responsavel varchar(60) NOT NULL,
	telefone varchar(15) NOT NULL,
	qtd_pessoas smallint NOT NULL,
	nome_instituicao varchar(60),
	usuario_cadastro integer NOT NULL,
	data_cadastro timestamp with time zone NOT NULL DEFAULT now(),
	CONSTRAINT visita_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE agenda_ciencia.visita OWNER TO postgres;
-- ddl-end --

-- object: id | type: COLUMN --
-- ALTER TABLE agenda_ciencia.evento DROP COLUMN IF EXISTS id CASCADE;
ALTER TABLE agenda_ciencia.evento ADD COLUMN id integer NOT NULL DEFAULT nextval('agenda_ciencia.evento_id_seq'::regclass);
-- ddl-end --


-- object: id | type: COLUMN --
-- ALTER TABLE agenda_ciencia.usuario DROP COLUMN IF EXISTS id CASCADE;
ALTER TABLE agenda_ciencia.usuario ADD COLUMN id integer NOT NULL DEFAULT nextval('agenda_ciencia.usuario_id_seq'::regclass);
-- ddl-end --


-- object: id | type: COLUMN --
-- ALTER TABLE agenda_ciencia.noticia DROP COLUMN IF EXISTS id CASCADE;
ALTER TABLE agenda_ciencia.noticia ADD COLUMN id integer NOT NULL DEFAULT nextval('agenda_ciencia.noticia_id_seq'::regclass);
-- ddl-end --


-- object: id | type: COLUMN --
-- ALTER TABLE agenda_ciencia.mural DROP COLUMN IF EXISTS id CASCADE;
ALTER TABLE agenda_ciencia.mural ADD COLUMN id integer NOT NULL DEFAULT nextval('agenda_ciencia.mural_id_seq'::regclass);
-- ddl-end --


-- object: id | type: COLUMN --
-- ALTER TABLE agenda_ciencia.visita DROP COLUMN IF EXISTS id CASCADE;
ALTER TABLE agenda_ciencia.visita ADD COLUMN id integer NOT NULL DEFAULT nextval('agenda_ciencia.visita_id_seq'::regclass);
-- ddl-end --




-- [ Created constraints ] --
-- object: evento_pk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_pk CASCADE;
ALTER TABLE agenda_ciencia.evento ADD CONSTRAINT evento_pk PRIMARY KEY (id);
-- ddl-end --

-- object: usuario_pk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.usuario DROP CONSTRAINT IF EXISTS usuario_pk CASCADE;
ALTER TABLE agenda_ciencia.usuario ADD CONSTRAINT usuario_pk PRIMARY KEY (id);
-- ddl-end --

-- object: noticia_pk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_pk CASCADE;
ALTER TABLE agenda_ciencia.noticia ADD CONSTRAINT noticia_pk PRIMARY KEY (id);
-- ddl-end --

-- object: mural_pk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.mural DROP CONSTRAINT IF EXISTS mural_pk CASCADE;
ALTER TABLE agenda_ciencia.mural ADD CONSTRAINT mural_pk PRIMARY KEY (id);
-- ddl-end --

-- object: visita_pk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_pk CASCADE;
ALTER TABLE agenda_ciencia.visita ADD CONSTRAINT visita_pk PRIMARY KEY (id);
-- ddl-end --



-- [ Created foreign keys ] --
-- object: usuario_tipo_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.usuario DROP CONSTRAINT IF EXISTS usuario_tipo_id_fk CASCADE;
ALTER TABLE agenda_ciencia.usuario ADD CONSTRAINT usuario_tipo_id_fk FOREIGN KEY (tipo)
REFERENCES agenda_ciencia.tipo_usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: evento_usuario_cadastro_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_usuario_cadastro_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.evento ADD CONSTRAINT evento_usuario_cadastro_usuario_id_fk FOREIGN KEY (usuario_cadastro)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: evento_usuario_alteracao_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.evento DROP CONSTRAINT IF EXISTS evento_usuario_alteracao_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.evento ADD CONSTRAINT evento_usuario_alteracao_usuario_id_fk FOREIGN KEY (usuario_alteracao)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: noticia_usuario_cadastro_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_usuario_cadastro_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.noticia ADD CONSTRAINT noticia_usuario_cadastro_usuario_id_fk FOREIGN KEY (usuario_cadastro)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: noticia_usuario_alteracao_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.noticia DROP CONSTRAINT IF EXISTS noticia_usuario_alteracao_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.noticia ADD CONSTRAINT noticia_usuario_alteracao_usuario_id_fk FOREIGN KEY (usuario_alteracao)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: mural_usuario_cadastro_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.mural DROP CONSTRAINT IF EXISTS mural_usuario_cadastro_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.mural ADD CONSTRAINT mural_usuario_cadastro_usuario_id_fk FOREIGN KEY (usuario_cadastro)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: mural_curtida_mural_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.mural_curtida DROP CONSTRAINT IF EXISTS mural_curtida_mural_id_fk CASCADE;
ALTER TABLE agenda_ciencia.mural_curtida ADD CONSTRAINT mural_curtida_mural_id_fk FOREIGN KEY (mural_id)
REFERENCES agenda_ciencia.mural (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: mural_curtida_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.mural_curtida DROP CONSTRAINT IF EXISTS mural_curtida_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.mural_curtida ADD CONSTRAINT mural_curtida_usuario_id_fk FOREIGN KEY (usuario_id)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: visita_periodo_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_periodo_id_fk CASCADE;
ALTER TABLE agenda_ciencia.visita ADD CONSTRAINT visita_periodo_id_fk FOREIGN KEY (periodo)
REFERENCES agenda_ciencia.periodo (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: visita_atracao_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_atracao_id_fk CASCADE;
ALTER TABLE agenda_ciencia.visita ADD CONSTRAINT visita_atracao_id_fk FOREIGN KEY (atracao)
REFERENCES agenda_ciencia.atracao (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: visita_tipo_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_tipo_id_fk CASCADE;
ALTER TABLE agenda_ciencia.visita ADD CONSTRAINT visita_tipo_id_fk FOREIGN KEY (tipo)
REFERENCES agenda_ciencia.tipo_visita (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: visita_usuario_cadastro_usuario_id_fk | type: CONSTRAINT --
-- ALTER TABLE agenda_ciencia.visita DROP CONSTRAINT IF EXISTS visita_usuario_cadastro_usuario_id_fk CASCADE;
ALTER TABLE agenda_ciencia.visita ADD CONSTRAINT visita_usuario_cadastro_usuario_id_fk FOREIGN KEY (usuario_cadastro)
REFERENCES agenda_ciencia.usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

